export class JWTPayload {
    id: string;
    username: string;
    email: string;
}
