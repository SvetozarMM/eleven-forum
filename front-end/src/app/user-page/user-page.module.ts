import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersModule } from '../users/users.module';
import { UserInfoBoxComponent } from './components/user-info-box/user-info-box.component';
import { NavBoxComponent } from './components/nav-box/nav-box.component';
import { HeaderUserPageComponent } from './components/header-user-page/header-user-page.component';
import { SidebarUserPageComponent } from './components/sidebar-user-page/sidebar-user-page.component';
import { MenuComponent } from './components/menu/menu.component';
import { MenuActionsComponent } from './components/menu-actions/menu-actions.component';
import { PostsModule } from '../posts/posts.module';
import { UserPageRouterModule } from './user-page-router.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    UserInfoBoxComponent,
    NavBoxComponent,
    HeaderUserPageComponent,
    SidebarUserPageComponent,
    MenuComponent,
    MenuActionsComponent,
  ],
  imports: [
    CommonModule,
    UsersModule,
    PostsModule,
    UserPageRouterModule,
    FormsModule,
  ],
  exports: [
    HeaderUserPageComponent,
    SidebarUserPageComponent,
  ]
})
export class UserPageModule { }
