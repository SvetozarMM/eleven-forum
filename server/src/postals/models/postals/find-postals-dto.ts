import { IsOptional, IsString, Length } from 'class-validator';

export class FindPostDTO {
    @IsOptional()
    @IsString()
    id?: string;

    @IsOptional()
    @IsString()
    @Length(1, 100)
    title?: string;

}
