import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutSectionComponent } from './components/about-section/about-section.component';



@NgModule({
  declarations: [AboutSectionComponent],
  imports: [
    CommonModule
  ]
})
export class AboutModule { }
