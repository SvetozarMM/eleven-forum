import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserDTO } from '../../models/user.dto';
import { CONFIG } from '../../config/config';
import { ResponseMessageDTO } from '../../models/response-msg.dto';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private readonly client: HttpClient) { }

  getAllUsers(): Observable<UserDTO[]> {
    return this.client.get<UserDTO[]>(`http://localhost:3000/users/all`);
  }

  getUserByName(username: string): Observable<UserDTO> {
    return this.client.get<UserDTO>(`http://localhost:3000/users?username=${username}`);
  }

  uploadUserAvatar(userId: string, file: File): Observable<UserDTO> {
    const formData = new FormData();
    formData.append('file', file);

    return this.client.post<UserDTO>(`${CONFIG.API_DOMAIN_NAME}/users/${userId}/avatar`, formData);
  }

  getUserFriends(): Observable<UserDTO[]> {
    return this.client.get<UserDTO[]>(`http://localhost:3000/users/friends`);
  }

  getUserActivity(user: UserDTO): Observable<any> {
    return this.client.get<any>(`http://localhost:3000/users/${user.id}/activity`);
  }

  updateUserBanstatus(user: UserDTO): Observable<UserDTO> {
    return this.client.put<UserDTO>(`http://localhost:3000/users/${user.id}/banstatus`, user.banstatus);
  }

  updateUserRoles(user: UserDTO): Observable<UserDTO> {
    return this.client.put<UserDTO>(`http://localhost:3000/users/${user.id}/roles`, user.roles);
  }

  deleteUser(user: UserDTO): Observable<ResponseMessageDTO> {
    return this.client.delete<ResponseMessageDTO>(`http://localhost:3000/users?username=${user.username}`);
  }

  addFriend(user: UserDTO): Observable<UserDTO> {
    return this.client.post<UserDTO>(`http://localhost:3000/users/${user.id}/friends`, user);
  }
}
