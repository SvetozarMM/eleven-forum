import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { HomePageModule } from './home-page/home-page.module';
import { UserPageModule } from './user-page/user-page.module';
import { UsersModule } from './users/users.module';
import { PostsModule } from './posts/posts.module';
import { CoreModule } from './core/core.module';
import { AppRouterModule } from './app-router.module';
import { AdminControlModule } from './admin-control/admin-control.module';
import { HttpClientModule } from '@angular/common/http';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptorService } from './auth/token-interceptor.service';
import { SpinnerIntercerptorService } from './interceptors/spinner-intercerptor.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FriendsModule } from './friends/friends.module';
import { ActivityModule } from './activity/activity.module';


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SharedModule,
    HomePageModule,
    UserPageModule,
    UsersModule,
    PostsModule,
    CoreModule,
    AppRouterModule,
    AdminControlModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    ActivityModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: SpinnerIntercerptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
