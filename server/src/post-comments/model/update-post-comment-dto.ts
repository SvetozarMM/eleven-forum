import { IsString, Length } from 'class-validator';

export class UpdatePostCommentDTO {
    @IsString()
    @Length(1)
    content: string;
}
