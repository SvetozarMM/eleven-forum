import { Component, Input } from '@angular/core';
import { Post } from '../../../models/post.model';

@Component({
  selector: 'app-post-box-small',
  templateUrl: './post-box-small.component.html',
  styleUrls: ['./post-box-small.component.css']
})
export class PostBoxSmallComponent {

  constructor() {}

  @Input() post: Post;

  openPost() {

  }

}
