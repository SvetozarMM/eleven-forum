import { Length, Matches } from 'class-validator';

export class UserLoginDTO {
    @Length(2, 25)
    username: string;

    @Matches(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/, {
        message:
          'The password must be minimum five characters, at least one letter and one number',
    })
    password: string;
}
