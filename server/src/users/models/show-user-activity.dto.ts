import { Expose, Transform } from 'class-transformer';
import { UserActivitiesType } from '../enums/user-activity-enum';
import { EntityType } from '../enums/entity.enum';

export class ShowUserActivityDto {

    @Expose()
    id: number;

    @Expose()
    public userActivity: UserActivitiesType;

    @Expose()
    public entity: EntityType;

    @Expose()
    public entityId: number;

    @Expose()
    public createdOn: Date;

    @Expose()
    @Transform((_, obj) => obj.user.username)
    public username: string;
}
