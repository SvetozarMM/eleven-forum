# "Eleven Forum"

### Description:
- An End-To-End project, discussion forum, organized by posts and comments. A discussion where a user can post, edit, like, flag and comment. Admins can manages users and moderate posts.
- Collaborated with Georgi.

<hr>

### Technologies:

#### Front-End:
- ###### Angular, TypeScript, CSS, HTML, Jest

#### Back-End:
- ###### NestJS, TypeORM, MySQL, Jest

#### Tools:
- ###### Git, ESLint, BEM, npm

<hr>

### In order to run the project follow these steps:
1. Clone the project from the Repository.
1. Open terminal and navigate to "front-end" folder and run: ``` npm install ```.
1. Then navigate to "server" folder and run: ``` npm install ```.
1. Download and setup a database: MySQL, MariaDB, Postgress will all do the job.
1. Create a database called **"forumdb"**.
1. In the root directory of the "server" folder create two files.
    - ```.env``` file with the following content
    ```
        PORT = 3000
        DB_TYPE = mysql
        DB_HOST = localhost
        DB_PORT = 3306
        DB_USERNAME = root
        DB_PASSWORD = Your Password from the Database!
        DB_DATABASE_NAME = forumdb
        JWT_SECRET = secretnost1
        JWT_EXPIRE_TIME = 3600s
    ``` 
    - ```ormconfig.json``` file with the following content
    ```
    {
        "type": "mysql",
        "host": "localhost",
        "port": 3306,
        "username": "root",
        "password": Your Password from the Database!,
        "database": "forumdb",
        "entities": [
            "src/data/entities/**/*.ts"
        ],
        "migrations": [
            "src/data/migration/**/*.ts"
        ],
        "cli": {
            "entitiesDir": "src/data/entities",
            "migrationsDir": "src/data/migration"
        }
    }
    ```
- #### Than we have to run the migrations. Follow this pattern:

1. To generate a new migration with name "Initial", run: 
    ```
    npm run typeorm -- migration:generate -n Initial
    ```

1. Apply the existing migrations to the database: 
    ```
    npm run typeorm -- migration:run
    ```

1. To create first 'admin' user run: 
    ```
    npm run seed
    ```

    ```
        Initial User(Admin) goes as follows:
        username = 'admin';
        password = 'Aaa123';
        email = 'admin@test.com';
    ```
<hr>

### Finally, to start application, please:

1. Start server - In terminal navigate to "server" folder and run:
    ```
    npm run start:dev
    ```

1. Start client at "localhost:4200" - In terminal navigate to "front-end" folder and run:

    ```
    ng serve --open
    ```

<hr>

### Tests:

- To test it yourself use:
```
    npm run test
```
- Or use "Majestic":
```
    npm run majestic
```
