import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBoxSmallComponent } from './user-box-small.component';

describe('UserBoxSmallComponent', () => {
  let component: UserBoxSmallComponent;
  let fixture: ComponentFixture<UserBoxSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBoxSmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBoxSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
