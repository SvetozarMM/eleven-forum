import { Expose, Transform } from 'class-transformer';
import { UserEntity } from '../../data/entities/user.entity';

export class ReturnPostCommentDTO {
  @Expose()
  id: string;

  @Expose()
  content: string;

  @Expose()
  createdOn: Date;

  @Expose()
  @Transform((_, obj) => obj.userCreator)
  userCreator: UserEntity;

  @Expose()
  @Transform((_, obj) => obj.votes.map((x: any) => x.username))
  votes: UserEntity[];

  isDeleted: boolean;
}
