import { PostCommentsService } from './post-comments.service';
import { PostsComments } from '../data/entities/post-comments.entity';
import { UserEntity } from '../data/entities/user.entity';
import { Posts } from '../data/entities/postal.entity';
import { TestingModule, Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { ReturnPostCommentDTO } from './model/return-post-comment-dto';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { CreatePostCommentDTO } from './model/create-post-comment-dto';
import { UpdatePostCommentDTO } from './model/update-post-comment-dto';

describe('PostCommentsService', () => {
  let service: PostCommentsService;
  let postCommentRepo: any;
  let usersRepo: any;
  let postsRepo: any;
  let comment: PostsComments;
  let expectedComment: Partial<ReturnPostCommentDTO>;
  let createComment: CreatePostCommentDTO;
  let updateComment: UpdatePostCommentDTO;
  let user: ShowUserDTO;

  beforeEach(async () => {
    postCommentRepo = {
      find() { return null; },
      findOne() { return null; },
      create() { return null; },
      save() { return null; },
      update() { return null; },
    };

    postsRepo = {
      findOne() { return null; },
    };

    usersRepo = {
      findOne() { return null; },
    };

    comment = {
      id: '123qwerty',
      content: 'Test content',
      createdOn: null,
      isDeleted: false,
      owner: null,
      userCreator: null,
      votes: null,
    };

    createComment = {
      content: 'Test content',
    };

    updateComment = {
      content: 'Test content',
    };

    expectedComment = {
      id: '123qwerty',
      content: 'Test content',
      createdOn: null,
    };

    user = {
      id: 'qwerty12345',
      username: 'TestName',
      email: 'testname@test.com',
      avatar: 'test.jpeg',
      roles: ['Admin'],
      banstatus: null,
      friends: [],
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostCommentsService,
        { provide: getRepositoryToken(PostsComments), useValue: postCommentRepo },
        { provide: getRepositoryToken(UserEntity), useValue: usersRepo },
        { provide: getRepositoryToken(Posts), useValue: postsRepo },
      ],
    }).compile();

    service = module.get<PostCommentsService>(PostCommentsService);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('all()', () => {
    it(`should call postsCommentRepository find(),
     once with correct filtering object if the default value for 'withDeleted' is used`, async () => {
      // Arrange
      const spy = jest
        .spyOn(postCommentRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      const expectedFilteredObject = { where: { isDeleted: false } };

      // Act
      await service.all();

      // Assert
      expect(spy).toBeCalledWith(expectedFilteredObject);
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository find(),
     once with no parameters if the default value for 'withDeleted' is not used`, async () => {
      // Arrange
      const spy = jest
        .spyOn(postCommentRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      const expectedFilteredObject = { where: { isDeleted: false } };

      // Act
      await service.all(true);

      // Assert
      expect(spy).not.toBeCalledWith(expectedFilteredObject);
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return transformed array of ReturnPostCommentDTO objects', async () => {
      // Arrange
      const spy = jest
        .spyOn(postCommentRepo, 'find')
        .mockReturnValue(Promise.resolve([comment]));

      // Act
      const result = await service.all(true);

      // Assert
      expect(result.length).toEqual(1);
      expect(result[0]).toBeInstanceOf(ReturnPostCommentDTO);
      expect(result[0]).toEqual(expectedComment);

      jest.clearAllMocks();
    });
  });

  describe('allOwn()', () => {
    it(`should call usersRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const spy = jest.spyOn(usersRepo, 'findOne');

      // Act
      await service.allOwn(user);

      // Assert
      expect(spy).toBeCalledWith({ where: { username: user.username, isDeleted: false } });
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found user is undefined (does not exist)`, async () => {
      // Arrange
      jest.spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.allOwn(user)).rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository find(),
     once with correct filtering object if the default value for 'withDeleted' is used`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostCommentRepositoryFindSpy = jest
        .spyOn(postCommentRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      // Act
      await service.allOwn(user);

      // Assert
      expect(mockedPostCommentRepositoryFindSpy)
        .toBeCalledWith({ where: { userCreator: mockedUserEntity, isDeleted: false} });
      expect(mockedPostCommentRepositoryFindSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository find(),
     once with no parameters if the default value for 'withDeleted' is not used`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostCommentRepositoryFindSpy = jest
        .spyOn(postCommentRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      // Act
      await service.allOwn(user, true);

      // Assert
      expect(mockedPostCommentRepositoryFindSpy)
        .not.toBeCalledWith({ where: { userCreator: mockedUserEntity, isDeleted: false} });
      expect(mockedPostCommentRepositoryFindSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return transformed array of ReturnPostCommentDTO objects', async () => {
      // Arrange
      const spy = jest
        .spyOn(postCommentRepo, 'find')
        .mockReturnValue(Promise.resolve([comment]));

      // Act
      const result = await service.allOwn(user, true);

      // Assert
      expect(result.length).toEqual(1);
      expect(result[0]).toBeInstanceOf(ReturnPostCommentDTO);
      expect(result[0]).toEqual(expectedComment);

      jest.clearAllMocks();
    });
  });

  describe('create()', () => {
    it(`should call usersRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedPost = new Posts();

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      const mockedPostCommentRepositoryCreateSpy = jest
        .spyOn(postCommentRepo, 'create')
        .mockReturnValue(comment);

      const spy = jest.spyOn(usersRepo, 'findOne');

      // Act
      await service.create(user, createComment, mockedPost.id);

      // Assert
      expect(spy).toBeCalledWith({ where: { username: user.username, isDeleted: false } });
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found user is undefined (does not exist)`, async () => {
      // Arrange
      const mockedPost = new Posts();

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      const mockedPostCommentRepositoryCreateSpy = jest
        .spyOn(postCommentRepo, 'create')
        .mockReturnValue(comment);

      jest.spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.create(user, createComment, mockedPost.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsRepository findOne(),
    once with correct parameters`, async () => {
      // Arrange
      const mockedPost = new Posts();
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      const mockedPostCommentRepositoryCreateSpy = jest
        .spyOn(postCommentRepo, 'create')
        .mockReturnValue(comment);

      // Act
      await service.create(user, createComment, mockedPost.id);

      // Assert
      expect(mockedPostRepositoryFindOneSpy)
        .toBeCalledWith({ where: { id: mockedPost.id, isDeleted: false } });
      expect(mockedPostRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
   });

    it(`should throw error if the found post is undefined (does not exist)`, async () => {
      // Arrange
      const mockedPost = new Posts();
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostCommentRepositoryCreateSpy = jest
        .spyOn(postCommentRepo, 'create')
        .mockReturnValue(comment);

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.create(user, createComment, mockedPost.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
   });

    it(`should throw error if the found post is 'Locked'`, async () => {
      // Arrange
      const mockedPost = new Posts();
      mockedPost.isLocked = true;
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostCommentRepositoryCreateSpy = jest
        .spyOn(postCommentRepo, 'create')
        .mockReturnValue(comment);

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      // Act & Assert
      expect(service.create(user, createComment, mockedPost.id))
        .rejects.toThrowError(`Locked post!`);

      jest.clearAllMocks();
    });

    it('should call postsCommentRepository create() once with correct parameters', async () => {
      // Arrange
      const mockedPost = new Posts();
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      const mockedPostCommentRepositoryCreateSpy = jest
        .spyOn(postCommentRepo, 'create')
        .mockReturnValue(comment);

      // Act
      await service.create(user, createComment, mockedPost.id);

      // Assert
      expect(mockedPostCommentRepositoryCreateSpy)
        .toBeCalledWith(createComment);
      expect(mockedPostCommentRepositoryCreateSpy)
        .toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postsCommentRepository save() once with correct parameters', async () => {
      // Arrange
      const mockedPost = new Posts();
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      const mockedPostCommentRepositoryCreateSpy = jest
        .spyOn(postCommentRepo, 'create')
        .mockReturnValue(comment);

      const mockedPostCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      await service.create(user, createComment, mockedPost.id);

      // Assert
      expect(mockedPostCommentRepositorySaveSpy)
        .toBeCalledWith(comment);
      expect(mockedPostCommentRepositorySaveSpy)
        .toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return transformed ReturnPostCommentDTO object', async () => {
      // Arrange
      const mockedPost = new Posts();
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      const mockedPostCommentRepositoryCreateSpy = jest
        .spyOn(postCommentRepo, 'create')
        .mockReturnValue(comment);

      const mockedPostCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      const result = await service.create(user, createComment, mockedPost.id);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostCommentDTO);
      expect(result).toEqual(expectedComment);

      jest.clearAllMocks();
    });
  });

  describe('update()', () => {
    it(`should call postsCommentRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      await service.update(user, comment.id, updateComment);

      // Assert
      expect(mockedPostsCommentRepositoryFindOneSpy)
        .toBeCalledWith({ where: { id: comment.id, isDeleted: false } });
      expect(mockedPostsCommentRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found comment is undefined (does not exist)`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.update(user, comment.id, updateComment))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository update(),
     if user role is 'Admin'`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositoryUpdateSpy = jest
        .spyOn(postCommentRepo, 'update');

      // Act
      await service.update(user, comment.id, updateComment);

      // Assert
      expect(mockedPostsCommentRepositoryUpdateSpy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository update(),
     if user role is 'Basic', but is the author of the comment`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const basicUserButAuthor: ShowUserDTO = {
        ...user,
        username: (await comment.userCreator).username,
        roles: ['Basic'],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositoryUpdateSpy = jest
        .spyOn(postCommentRepo, 'update');

      // Act
      await service.update(basicUserButAuthor, comment.id, updateComment);

      // Assert
      expect(mockedPostsCommentRepositoryUpdateSpy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository update(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositoryUpdateSpy = jest
        .spyOn(postCommentRepo, 'update');

      // Act
      await service.update(user, comment.id, updateComment);

      // Assert
      expect(mockedPostsCommentRepositoryUpdateSpy)
        .toBeCalledWith(comment.id, updateComment);
      expect(mockedPostsCommentRepositoryUpdateSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if user role is 'Basic',
     and the author of the comment is someone else`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const basicUserButAuthor: ShowUserDTO = {
        ...user,
        roles: ['Basic'],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositoryUpdateSpy = jest
        .spyOn(postCommentRepo, 'update');

      // Act & Assert
      expect(mockedPostsCommentRepositoryUpdateSpy).not.toHaveBeenCalled();
      expect(service.update(basicUserButAuthor, comment.id, updateComment))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it('should return transformed ReturnPostCommentDTO object', async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositoryUpdateSpy = jest
        .spyOn(postCommentRepo, 'update')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      const result = await service.update(user, comment.id, updateComment);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostCommentDTO);
      expect(result).toEqual(expectedComment);

      jest.clearAllMocks();
    });
  });

  describe('delete()', () => {
    it(`should call postsCommentRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      await service.delete(user, comment.id);

      // Assert
      expect(mockedPostsCommentRepositoryFindOneSpy)
        .toBeCalledWith({ where: { id: comment.id, isDeleted: false } });
      expect(mockedPostsCommentRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found comment is undefined (does not exist)`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.delete(user, comment.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository save(),
     if user role is 'Admin'`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save');

      // Act
      await service.delete(user, comment.id);

      // Assert
      expect(mockedPostsCommentRepositorySaveSpy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository save(),
     if user role is 'Basic', but is the author of the comment`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const basicUserButAuthor: ShowUserDTO = {
        ...user,
        username: (await comment.userCreator).username,
        roles: ['Basic'],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save');

      // Act
      await service.delete(basicUserButAuthor, comment.id);

      // Assert
      expect(mockedPostsCommentRepositorySaveSpy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository save(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save');

      // Act
      await service.delete(user, comment.id);

      // Assert
      expect(mockedPostsCommentRepositorySaveSpy)
        .toBeCalledWith({ ...comment, isDeleted: true });
      expect(mockedPostsCommentRepositorySaveSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if user role is 'Basic',
     and the author of the comment is someone else`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const basicUserButAuthor: ShowUserDTO = {
        ...user,
        roles: ['Basic'],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save');

      // Act & Assert
      expect(mockedPostsCommentRepositorySaveSpy).not.toHaveBeenCalled();
      expect(service.delete(basicUserButAuthor, comment.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it('should return transformed ReturnPostCommentDTO object', async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      comment.userCreator = mockedUserEntity;

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      const result = await service.delete(user, comment.id);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostCommentDTO);
      expect(result).toEqual(expectedComment);

      jest.clearAllMocks();
    });
  });

  describe('likeComment()', () => {
    it(`should call postsCommentRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      comment = {
        ...comment,
        votes: [mockedUserEntity],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      await service.likeComment(user.id, comment.id);

      // Assert
      expect(mockedPostsCommentRepositoryFindOneSpy)
        .toBeCalledWith({ where: { id: comment.id, isDeleted: false } });
      expect(mockedPostsCommentRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found comment is undefined (does not exist)`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      comment = {
        ...comment,
        votes: [mockedUserEntity],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.likeComment(user.id, comment.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call usersRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      comment = {
        ...comment,
        votes: [mockedUserEntity],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      await service.likeComment(user.id, comment.id);

      // Assert
      expect(mockedUsersRepositoryFindOneSpy)
        .toBeCalledWith({ where: { id: user.id, isDeleted: false } });
      expect(mockedUsersRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found user is undefined (does not exist)`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      comment = {
        ...comment,
        votes: [mockedUserEntity],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      // Act & Assert
      expect(service.likeComment(user.id, comment.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsCommentRepository save(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      comment = {
        ...comment,
        votes: [mockedUserEntity],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save');

      // Act
      await service.likeComment(user.id, comment.id);

      // Assert
      expect(mockedPostsCommentRepositorySaveSpy)
        .toBeCalledWith(comment);
      expect(mockedPostsCommentRepositorySaveSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should return transformed ReturnPostCommentDTO object`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      comment = {
        ...comment,
        votes: [mockedUserEntity],
      };

      const mockedPostsCommentRepositoryFindOneSpy = jest
        .spyOn(postCommentRepo, 'findOne')
        .mockReturnValue(Promise.resolve(comment));

      const mockedPostsCommentRepositorySaveSpy = jest
        .spyOn(postCommentRepo, 'save')
        .mockReturnValue(Promise.resolve(comment));

      // Act
      const result = await service.likeComment(user.id, comment.id);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostCommentDTO);
      expect(result).toEqual(expectedComment);

      jest.clearAllMocks();
    });
  });
});
