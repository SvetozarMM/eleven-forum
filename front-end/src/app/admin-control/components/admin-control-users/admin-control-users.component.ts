import { Component, OnInit } from '@angular/core';
import { UserDTO } from '../../../models/user.dto';
import { UsersService } from '../../../core/services/users.service';
import { NotificatorService } from '../../../core/services/notificator.service';

@Component({
  selector: 'app-admin-control-users',
  templateUrl: './admin-control-users.component.html',
  styleUrls: ['./admin-control-users.component.css']
})
export class AdminControlUsersComponent implements OnInit {

  users: UserDTO[];

  constructor(
    private readonly usersService: UsersService,
    private readonly notificator: NotificatorService,
  ) { }

  ngOnInit(): void {
    this.usersService
      .getAllUsers()
      .subscribe({
        next: (data: UserDTO[]) => (this.users = data),
        error: e => console.log(e)
      });
  }

  updateBanstatus($event): void {
    this.usersService.updateUserBanstatus($event)
        .subscribe({
          next: data => {
            const index = this.users.indexOf($event);
            this.users[index] = data;
          },
        });
  }
  updateRoles($event) {
    this.usersService.updateUserRoles($event)
      .subscribe({
        next: data => {
          const index = this.users.indexOf($event);
          this.users[index] = data;
        },
      });
  }

  deleteUser($event): void {
    this.usersService.deleteUser($event)
      .subscribe({
        next: data => {
          this.users = this.users.filter(user => user.id !== $event.id);
          this.notificator.success(data.message);
        },
      });
  }
}
