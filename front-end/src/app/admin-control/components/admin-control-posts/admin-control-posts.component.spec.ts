import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminControlPostsComponent } from './admin-control-posts.component';

describe('AdminControlPostsComponent', () => {
  let component: AdminControlPostsComponent;
  let fixture: ComponentFixture<AdminControlPostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminControlPostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminControlPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
