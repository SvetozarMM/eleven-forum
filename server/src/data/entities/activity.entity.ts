import { Entity, BaseEntity, PrimaryGeneratedColumn, CreateDateColumn, ManyToOne, Column } from 'typeorm';
import { UserEntity } from './user.entity';
import { UserActivitiesType } from '../../users/enums/user-activity-enum';
import { EntityType } from '../../users/enums/entity.enum';

@Entity('activity')
export class UserActivity {

    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column('enum', { enum: UserActivitiesType })
    userActivity: UserActivitiesType;

    @Column('enum', { enum: EntityType })
    entity: EntityType;

    @Column({ nullable: false })
    entityId: string;

    @CreateDateColumn()
    public createdOn: Date;

    @ManyToOne(
        type => UserEntity,
        user => user.activity,
    )
    public user: UserEntity;
}
