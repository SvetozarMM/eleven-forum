import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from 'src/app/models/user.dto';
import { AuthService } from 'src/app/core/services/auth.service';
import { CONFIG } from '../../../config/config';
import { UsersService } from '../../../core/services/users.service';

@Component({
  selector: 'app-pref-section',
  templateUrl: './pref-section.component.html',
  styleUrls: ['./pref-section.component.css']
})
export class PrefSectionComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  userAvatarSrcPrefix: string = CONFIG.USER_AVATAR_SRC_PREFIX;
  defaultUserAvatarImagePath: string = CONFIG.DEFAULT_USER_AVATAR_IMAGE_PATH;
  user: UserDTO;

  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  ngOnInit() {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  onFileSelected(file: File): void {
    this.usersService
      .uploadUserAvatar(this.user.id, file)
      .subscribe((updatedUser: UserDTO) =>
        this.authService.emitUserData(updatedUser)
      );
  }

}
