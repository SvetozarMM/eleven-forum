import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PostsSectionComponent } from '../posts/components/posts-section/posts-section.component';
import { AdminControlSectionComponent } from '../admin-control/components/admin-control-section/admin-control-section.component';
import { AdminControlUsersComponent } from '../admin-control/components/admin-control-users/admin-control-users.component';
import { AdminControlPostsComponent } from '../admin-control/components/admin-control-posts/admin-control-posts.component';
import { AdminControlCommentsComponent } from '../admin-control/components/admin-control-comments/admin-control-comments.component';
import { AuthGuard } from '../auth/auth.guard';
import { PrefSectionComponent } from '../preferences/components/pref-section/pref-section.component';
import { AboutSectionComponent } from '../about/components/about-section/about-section.component';
import { ActivitySectionComponent } from '../activity/components/activity-section/activity-section.component';
import { FriendsComponent } from '../friends/components/friends/friends.component';
import { ActivityPostsComponent } from '../activity/components/activity-posts/activity-posts.component';
import { ActivityCommentsComponent } from '../activity/components/activity-comments/activity-comments.component';

const routes: Routes = [
    { path: 'session/posts', component: PostsSectionComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'session/friends', component: FriendsComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'session/activity',
        children: [
            {
                path: '',
                redirectTo: 'all',
                pathMatch: 'full',
            },
            {
                path: 'all',
                component: ActivitySectionComponent,
            },
            {
                path: 'posts',
                component: ActivityPostsComponent,
            },
            {
                path: 'comments',
                component: ActivityCommentsComponent,
            },
        ],
        canActivate: [AuthGuard],
    },
    { path: 'session/about', component: AboutSectionComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'session/preferences', component: PrefSectionComponent, pathMatch: 'full', canActivate: [AuthGuard] },
    { path: 'session/admin-control',
        children: [
            {
                path: '',
                redirectTo: 'users',
                pathMatch: 'full',
            },
            {
                path: 'users',
                component: AdminControlUsersComponent,
            },
            {
                path: 'posts',
                component: AdminControlPostsComponent,
            },
            {
                path: 'comments',
                component: AdminControlCommentsComponent,
            },
        ],
        canActivate: [AuthGuard],
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class UserPageRouterModule {}
