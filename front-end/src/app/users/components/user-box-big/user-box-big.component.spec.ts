import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBoxBigComponent } from './user-box-big.component';

describe('UserBoxBigComponent', () => {
  let component: UserBoxBigComponent;
  let fixture: ComponentFixture<UserBoxBigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBoxBigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBoxBigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
