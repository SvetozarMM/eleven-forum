import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarUserPageComponent } from './sidebar-user-page.component';

describe('SidebarUserPageComponent', () => {
  let component: SidebarUserPageComponent;
  let fixture: ComponentFixture<SidebarUserPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarUserPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarUserPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
