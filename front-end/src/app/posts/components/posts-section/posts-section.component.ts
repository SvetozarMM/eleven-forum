import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../../models/post.model';
import { PostsService } from '../../posts.service';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { AuthService } from '../../../core/services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-posts-section',
  templateUrl: './posts-section.component.html',
  styleUrls: ['./posts-section.component.css']
})
export class PostsSectionComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;
  posts: Post[] = [];
  postTitle: string;
  isSearchedResultOpen = false;

  constructor(
    private readonly postsService: PostsService,
    private readonly authService: AuthService,
    private readonly router: Router,
  ) { }

  ngOnInit(): void {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.postsService
      .getAllPosts()
      .subscribe({
        next: (data: Post[]) => (this.posts = data),
        error: e => console.log(e)
      });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  likePost($event): void {
    if (!$event.votes.includes(this.user.username) && !$event.isLocked) {
      this.postsService.likePost($event)
        .subscribe({
          next: data => {
            const index = this.posts.indexOf($event);
            this.posts[index] = data;
          },
          error: e => console.log(e)
        });
    }
  }

  deletePost($event): void {
    this.postsService.deletePost($event.id)
      .subscribe({
        next: data => {
          this.posts = this.posts.filter(post => post.id !== $event.id);
        },
        error: e => console.log(e)
      });
  }

  lockUnlockPost($event): void {
    if ($event.isLocked) {
      this.postsService.unlockPost($event)
        .subscribe({
          next: data => {
            const index = this.posts.indexOf($event);
            this.posts[index] = data;
          },
          error: e => console.log(e)
        });
    } else {
      this.postsService.lockPost($event)
        .subscribe({
          next: data => {
            const index = this.posts.indexOf($event);
            this.posts[index] = data;
          },
          error: e => console.log(e)
        });
    }
  }

  updatePost($event): void {
    this.postsService.updatePost($event)
        .subscribe({
          next: data => {
            const index = this.posts.indexOf($event);
            this.posts[index] = data;
          },
          error: e => console.log(e)
        });
  }

  createPost($event): void {
    this.postsService.createPost($event)
      .subscribe({
        next: data => {
          this.posts.push(data);
        },
        error: e => console.log(e)
      });
  }

  searchPost() {
    this.postsService.getPostByTitle(this.postTitle)
      .subscribe({
        next: data => {
          this.isSearchedResultOpen = true;
          this.posts = data;
          this.postTitle = '';
        },
      });
  }

  closeSearchedResultBox() {
    this.isSearchedResultOpen = false;
    this.postsService
      .getAllPosts()
      .subscribe({
        next: (data: Post[]) => (this.posts = data),
        error: e => console.log(e)
      });
  }

}
