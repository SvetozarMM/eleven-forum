import { Component, OnInit } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from '../../../posts/posts.service';

@Component({
  selector: 'app-sidebar-user-page',
  templateUrl: './sidebar-user-page.component.html',
  styleUrls: ['./sidebar-user-page.component.css']
})
export class SidebarUserPageComponent implements OnInit {

  posts: Post[];

  constructor(
    private readonly route: ActivatedRoute,
    private readonly postsService: PostsService,
  ) { }

  ngOnInit(): void {
    this.postsService
      .getAllPosts()
      .subscribe((data: Post[]) => (this.posts = data));

  }

}
