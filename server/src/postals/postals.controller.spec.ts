import { PostalsController } from './postals.controller';
import { PostalsService } from './postals.service';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { CreatePostDTO } from './models/postals/create-postals-dto';
import { TestingModule, Test } from '@nestjs/testing';
import { async } from 'rxjs/internal/scheduler/async';
import { FindPostDTO } from './models/postals/find-postals-dto';
import { ReturnPostDTO } from './models/postals/return-postals-dto';
import { Posts } from '../data/entities/postal.entity';

describe('PostsController', () => {
  let controller: PostalsController;
  let postsService: Partial<PostalsService>;
  let user: ShowUserDTO;
  let post: CreatePostDTO;
  let findPost: FindPostDTO;
  let expectedPost: ReturnPostDTO;
  let options: Partial<Posts>;

  beforeEach(async () => {
    postsService = {
      all() { return null; },
      allOwnByOptions() { return null; },
      allOwn() { return null; },
      create() { return null; },
      update() { return null; },
      delete() { return null; },
      likePost() { return null; },
      lockUnlockPost() { return null; },
    };

    user = {
      id: 'qwerty12345',
      username: 'TestName',
      email: 'testname@test.com',
      avatar: 'test.jpeg',
      roles: ['Basic', 'Admin'],
      banstatus: { id: 1, isBanned: false, description: ''},
    };

    post = {
      title: 'Test Title',
      content: 'Test Content',
    };

    findPost = {
      id: 'qwerty12345',
      title: 'Test Title',
    };

    options = {
      id: 'qwerty12345',
      title: 'Test Title',
    };

    expectedPost = {
      id: 'qwerty12345',
      title: 'Test Title',
      content: 'Test Content',
      createdOn: null,
      isLocked: false,
      isDeleted: false,
    };

    const module: TestingModule = await Test.createTestingModule({
        controllers: [PostalsController],
        providers: [
            {
                provide: PostalsService,
                useValue: postsService,
            },
        ],
    }).compile();

    controller = module.get<PostalsController>(PostalsController);
  });

  it('should be defined', () => {
      // Arrange & Act & Assert
      expect(controller).toBeDefined();
  });

  describe('allPosts()', () => {
    it('should call postsService all(), once', async () => {
      // Arrange
      const spy = jest.spyOn(postsService, 'all');

      // Act
      await controller.allPost();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return the result from postsService all()', async () => {
      // Arrange
      const spy = jest
        .spyOn(postsService, 'all')
        .mockReturnValue(Promise.resolve([expectedPost]));

      // Act
      const response = await controller.allPost();

      // Assert
      expect(response).toEqual([expectedPost]);

      jest.clearAllMocks();
    });
  });

  describe('allOwnPosts()', () => {
    it(`should call postsService allOwnByOptions(),
      once with correct parameters`, async () => {
      // Arrange
      const spy = jest.spyOn(postsService, 'allOwnByOptions');

      // Act
      await controller.allOwnPosts(findPost, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user, options);
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsService allOwn(),
      once with correct parameters`, async () => {
      // Arrange
      const mockedFindPost = {};
      const spy = jest.spyOn(postsService, 'allOwn');

      // Act
      await controller.allOwnPosts(mockedFindPost, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user);
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return correct object', async () => {
      // Arrange
      const spy = jest
        .spyOn(postsService, 'allOwnByOptions')
        .mockReturnValue(Promise.resolve([expectedPost]));

      // Act
      const response = await controller.allOwnPosts(findPost, user);

      // Assert
      expect(response).toEqual([expectedPost]);

      jest.clearAllMocks();
    });
  });

  describe('create()', () => {
    it('should call postsService create(), once', async () => {
      // Arrange
      const spy = jest.spyOn(postsService, 'create');

      // Act
      await controller.create(post, user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postsService create() with correct parameters', async () => {
      // Arrange
      const spy = jest.spyOn(postsService, 'create');

      // Act
      await controller.create(post, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(post, user);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const expectedResult = { message: `Post created!` };

      jest.spyOn(postsService, 'create');

      // Act
      const response = await controller.create(post, user);

      // Assert
      expect(response).toEqual(expectedResult);
    });
  });

  describe('update()', () => {
    it('should call postsService update(), once', async () => {
      // Arrange
      const id: string = 'qwerty12345';
      const spy = jest.spyOn(postsService, 'update');

      // Act
      await controller.update(post, id, user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postsService update() with correct parameters', async () => {
      // Arrange
      const id: string = 'qwerty12345';
      const spy = jest.spyOn(postsService, 'update');

      // Act
      await controller.update(post, id, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user, id, post);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const id: string = 'qwerty12345';
      const expectedResult = { message: `Post updated!` };

      jest.spyOn(postsService, 'update');

      // Act
      const response = await controller.update(post, id, user);

      // Assert
      expect(response).toEqual(expectedResult);
    });
  });

  describe('likePost()', () => {
    it('should call postsService likePost(), once', async () => {
      // Arrange
      const id: string = '12345qwerty';
      const spy = jest.spyOn(postsService, 'likePost');

      // Act
      await controller.likePost(id, user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postsService likePost() with correct parameters', async () => {
      // Arrange
      const id: string = '12345qwerty';
      const spy = jest.spyOn(postsService, 'likePost');

      // Act
      await controller.likePost(id, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user.id, id);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const id: string = '12345qwerty';
      const expectedResult = { message: `U've liked it!` };

      jest.spyOn(postsService, 'likePost');

      // Act
      const response = await controller.likePost(id, user);

      // Assert
      expect(response).toEqual(expectedResult);
    });
  });

  describe('lockUnlockPost()', () => {
    it(`should call postsService lockUnlockPost(),
    once with correct parameters`, async () => {
      // Arrange
      const id: string = '12345qwerty';
      const status: string = '1';
      const spy = jest
        .spyOn(postsService, 'lockUnlockPost')
        .mockReturnValue(Promise.resolve(expectedPost));

      // Act
      await controller.lockUnlockPost(id, status);

      // Assert
      expect(spy).toHaveBeenCalledWith(id, status);
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should return correct message if status is '1' - locked`, async () => {
      // Arrange
      const id: string = '12345qwerty';
      const status: string = '1';
      const expectedResult = { message: `Post locked!` };
      const expectedResultWithIsLockedTrue: ReturnPostDTO = {
        ...expectedPost,
        isLocked: true,
      };

      const spy = jest
        .spyOn(postsService, 'lockUnlockPost')
        .mockReturnValue(Promise.resolve(expectedResultWithIsLockedTrue));

      // Act
      const response = await controller.lockUnlockPost(id, status);

      // Assert
      expect(response).toEqual(expectedResult);
    });

    it(`should return correct message if status is '-1' - unlocked`, async () => {
      // Arrange
      const id: string = '12345qwerty';
      const status: string = '-1';
      const expectedResult = { message: `Post unlocked!` };

      const spy = jest
        .spyOn(postsService, 'lockUnlockPost')
        .mockReturnValue(Promise.resolve(expectedPost));

      // Act
      const response = await controller.lockUnlockPost(id, status);

      // Assert
      expect(response).toEqual(expectedResult);
    });
  });

  describe('delete()', () => {
    it('should call postsService delete(), once', async () => {
      // Arrange
      const id: string = '12345qwerty';
      const spy = jest.spyOn(postsService, 'delete');

      // Act
      await controller.delete(id, user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });
    it('should call postsService delete() with correct parameters', async () => {
      // Arrange
      const id: string = '12345qwerty';
      const spy = jest.spyOn(postsService, 'delete');

      // Act
      await controller.delete(id, user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user, id);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const id: string = '12345qwerty';
      const expectedResult = { message: `Post deleted!` };

      jest.spyOn(postsService, 'delete');

      // Act
      const response = await controller.delete(id, user);

      // Assert
      expect(response).toEqual(expectedResult);

      jest.clearAllMocks();
    });
  });
});
