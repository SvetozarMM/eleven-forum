import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminControlSectionComponent } from './admin-control-section.component';

describe('AdminControlSectionComponent', () => {
  let component: AdminControlSectionComponent;
  let fixture: ComponentFixture<AdminControlSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminControlSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminControlSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
