import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrefSectionComponent } from './pref-section.component';

describe('PrefSectionComponent', () => {
  let component: PrefSectionComponent;
  let fixture: ComponentFixture<PrefSectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrefSectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrefSectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
