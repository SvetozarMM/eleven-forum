import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostBoxBigComponent } from './post-box-big.component';

describe('PostBoxBigComponent', () => {
  let component: PostBoxBigComponent;
  let fixture: ComponentFixture<PostBoxBigComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostBoxBigComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostBoxBigComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
