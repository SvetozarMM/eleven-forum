import { IsString, Length } from 'class-validator';

export class CreatePostCommentDTO {
    @IsString()
    @Length(1)
    content: string;
}
