import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommentDTO } from '../../models/comment.dto';
import { Observable } from 'rxjs';
import { CreateCommentDTO } from '../../models/create-comment.dto';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  constructor(private readonly client: HttpClient) { }

  getAllComments(): Observable<CommentDTO[]> {
    return this.client.get<CommentDTO[]>(`http://localhost:3000/comments/all`);
  }
  getAllOwnComments(): Observable<CommentDTO[]> {
    return this.client.get<CommentDTO[]>(`http://localhost:3000/comments`);
  }

  createComment(comment: CreateCommentDTO, postId: string): Observable<CommentDTO> {
    return this.client.post<CommentDTO>(`http://localhost:3000/postals/${postId}/comment`, comment);
  }

  updateComment(comment: CommentDTO): Observable<CommentDTO> {
    return this.client.put<CommentDTO>(`http://localhost:3000/comment/${comment.id}`, comment);
  }

  deleteComment(commentId: string): Observable<CommentDTO> {
    return this.client.delete<CommentDTO>(`http://localhost:3000/comment/${commentId}`);
  }

  likeComment(comment: CommentDTO): Observable<CommentDTO> {
    return this.client.put<CommentDTO>(`http://localhost:3000/comment/${comment.id}/like`, comment);
  }

}
