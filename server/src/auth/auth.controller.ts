import { Controller, Post, Body, Delete, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserLoginDTO } from '../users/models/user-login-dto';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { Token } from '../common/decorators/token.decorator';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService,
    ) {}

    @Post()
    async login(@Body() user: UserLoginDTO): Promise<{ token: string }> {
        return await this.authService.login(user);
    }

    @Delete()
    @UseGuards(AuthGuardWithBlacklisting)
    public async logoutUser(@Token() token: string) {
        this.authService.blacklistToken(token);

        return {
         msg: 'Successful logout!',
     };
    }
}
