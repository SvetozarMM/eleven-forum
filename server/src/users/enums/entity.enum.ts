export enum EntityType {
  User = 'User',
  Post = 'Post',
  Comment = 'Comment',
}