import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ShowUserDTO } from '../../users/models/show-user.dto';
import { UsersService } from '../../users/users.service';
import { ConfigService } from '@nestjs/config';
import { PassportModule } from '@nestjs/passport';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly userService: UsersService,
    configService: ConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET'),
      ignoreExpiration: false,
    });
  }

  public async validate(payload: ShowUserDTO): Promise<ShowUserDTO> {
    const user = await this.userService.findByName(payload.username);

    if (!user) {
      throw new UnauthorizedException();
    }

    return user;
  }
}
