import { HttpClientModule, HttpClient } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { CommentsService } from './comments.service';
import { CommentDTO } from '../../models/comment.dto';
import { UserDTO } from '../../models/user.dto';

describe('CommentsService', () => {
  let httpClient;
  let service: CommentsService;
  const API_DOMAIN_NAME = 'http://localhost:3000';
  let mockedComment: CommentDTO;
  let mockedUser: UserDTO;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get() {},
      post() {},
      put() {},
      delete() {}
    };

    mockedUser  = {
        id: '123qwe',
        username: 'user',
        email: 'userEmail',
        avatar: 'avatar',
        roles: ['Basic', 'Admin'],
        posts: [],
        comments: [],
        banstatus: {
          description: '',
          isBanned: false
        },
        likedPosts: [],
        likedComments: [],
        friends: [],
      };

    mockedComment = {
        id: '123qwe',
        content: 'test',
        createdOn: new Date(),
        owner: 'post1',
        userCreator: mockedUser,
        votes: []
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [CommentsService]
    }).overrideProvider(HttpClient, { useValue: httpClient });

    service = TestBed.inject(CommentsService);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('getAllComments()', () => {
    it('should call the httpClient "get()" method once with correct parameters', done => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comments/all`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'get')
        .mockReturnValue(returnValue);

      // Act & Assert
      service.getAllComments().subscribe(() => {
        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient "get()" method', () => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comments/all`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'get')
        .mockReturnValue(returnValue);

      // Act
      const result = service.getAllComments();

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('getAllOwnComments()', () => {
    it('should call the httpClient "get()" method once with correct parameters', done => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comments`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'get')
        .mockReturnValue(returnValue);

      // Act & Assert
      service.getAllOwnComments().subscribe(() => {
        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('should return the result from the httpClient "get()" method', () => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comments`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'get')
        .mockReturnValue(returnValue);

      // Act
      const result = service.getAllOwnComments();

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('createComment()', () => {
    it('should call the httpClient "post()" method once with correct parameters', done => {
      // Arrange
      const postId = 'id';
      const url = `${API_DOMAIN_NAME}/postals/${postId}/comment`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'post')
        .mockReturnValue(returnValue);

      // Act & Assert
      service.createComment(mockedComment, postId).subscribe(() => {
        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith(url, mockedComment);

        done();
      });
    });

    it('should return the result from the httpClient "post()" method', () => {
      // Arrange
      const postId = 'id';
      const url = `${API_DOMAIN_NAME}/postals/${postId}/comment`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'post')
        .mockReturnValue(returnValue);

      // Act
      const result = service.createComment(mockedComment, postId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('updateComment()', () => {
    it(`should call the httpClient "put()" method once with correct parameters`, done => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comment/${mockedComment.id}`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'put')
        .mockReturnValue(returnValue);

      // Act & Assert
      service.updateComment(mockedComment).subscribe(() => {
        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith(url, mockedComment);

        done();
      });
    });

    it(`should return the result from the httpClient "put()" method`, () => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comment/${mockedComment.id}`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'put')
        .mockReturnValue(returnValue);

      // Act
      const result = service.updateComment(mockedComment);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('deleteComment()', () => {
    it(`should call the httpClient "delete()" method once with correct parameters`, done => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comment/${mockedComment.id}`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'delete')
        .mockReturnValue(returnValue);

      // Act & Assert
      service.deleteComment(mockedComment.id).subscribe(() => {
        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it(`should return the result from the httpClient "delete()" method`, () => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comment/${mockedComment.id}`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'delete')
        .mockReturnValue(returnValue);

      // Act
      const result = service.deleteComment(mockedComment.id);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('likeComment()', () => {
    it(`should call the httpClient "put()" method once with correct parameters`, done => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comment/${mockedComment.id}/like`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'put')
        .mockReturnValue(returnValue);

      // Act & Assert
      service.likeComment(mockedComment).subscribe(() => {
        expect(spy).toHaveBeenCalled();
        expect(spy).toHaveBeenCalledWith(url, mockedComment);

        done();
      });
    });

    it(`should return the result from the httpClient "put()" method`, () => {
      // Arrange
      const url = `${API_DOMAIN_NAME}/comment/${mockedComment.id}/like`;
      const returnValue = of('test');

      const spy = jest
        .spyOn(httpClient, 'put')
        .mockReturnValue(returnValue);

      // Act
      const result = service.likeComment(mockedComment);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });
});
