import { Component, OnInit, OnDestroy } from '@angular/core';
import { AddPostService } from '../../../core/services/add-post.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-nav-box',
  templateUrl: './nav-box.component.html',
  styleUrls: ['./nav-box.component.css']
})
export class NavBoxComponent implements OnInit, OnDestroy {

  isCreateBoxOpen: boolean;
  private userSubscription: Subscription;
  public user: UserDTO;

  constructor(
    private readonly addService: AddPostService,
    private readonly router: Router,
    private readonly authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.addService.currentIsCreatedBoxOpen
      .subscribe(value => this.isCreateBoxOpen = value,
    );
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  showCreateBox() {
    this.addService.show();
    this.router.navigate(['/session/posts']);
  }

}
