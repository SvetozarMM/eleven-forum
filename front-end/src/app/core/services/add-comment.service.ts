import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddCommentService {

  private isCreateCommentBoxOpen = new BehaviorSubject(false);
  currentIsCreatedCommentBoxOpen = this.isCreateCommentBoxOpen.asObservable();

  constructor() { }

  show() {
    this.isCreateCommentBoxOpen.next(true);
  }

  hide() {
    this.isCreateCommentBoxOpen.next(false);
  }
}
