import { PostalsService } from './postals.service';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { Posts } from '../data/entities/postal.entity';
import { ReturnPostDTO } from './models/postals/return-postals-dto';
import { CreatePostDTO } from './models/postals/create-postals-dto';
import { UpdatePostDTO } from './models/postals/update-postals-dto';
import { TestingModule, Test } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { UserEntity } from '../data/entities/user.entity';
import { async } from 'rxjs/internal/scheduler/async';
import { ForumSystemError } from '../common/exceptions/forum-system.error';

describe('PostalsService', () => {
  let service: PostalsService;
  let usersRepo: any;
  let postsRepo: any;
  let post: Posts;
  let expectedPost: Partial<ReturnPostDTO>;
  let createPost: CreatePostDTO;
  let updatePost: UpdatePostDTO;
  let user: ShowUserDTO;
  let options: Partial<Posts>;

  beforeEach(async () => {
    postsRepo = {
      find() { return null; },
      findOne() { return null; },
      create() { return null; },
      save() { return null; },
      update() { return null; },
    };

    usersRepo = {
      findOne() { return null; },
    };

    post = {
      id: '123qwerty',
      title: 'Test Title',
      content: 'Test content',
      createdOn: null,
      isDeleted: false,
      isLocked: false,
      comments: null,
      userCreator: null,
      votes: null,
    };

    createPost = {
      title: 'Test Title',
      content: 'Test content',
    };

    updatePost = {
      title: 'Test Title',
      content: 'Test content',
    };

    expectedPost = {
      id: '123qwerty',
      title: 'Test Title',
      content: 'Test content',
      createdOn: null,
      isLocked: false,
    };

    user = {
      id: 'qwerty12345',
      username: 'TestName',
      email: 'testname@test.com',
      avatar: 'test.jpeg',
      roles: ['Admin'],
      banstatus: null,
    };

    options = {
      id: '123qwerty',
      title: 'Test Title',
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PostalsService,
        { provide: getRepositoryToken(UserEntity), useValue: usersRepo },
        { provide: getRepositoryToken(Posts), useValue: postsRepo },
      ],
    }).compile();

    service = module.get<PostalsService>(PostalsService);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('all()', () => {
    it(`should call postsRepository find(),
     once with correct filtering object if the default value for 'withDeleted' is used`, async () => {
      // Arrange
      const spy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([expectedPost]));

      const expectedFilteredObject = { where: { isDeleted: false } };

      // Act
      await service.all();

      // Assert
      expect(spy).toBeCalledWith(expectedFilteredObject);
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsRepository find(),
     once with no parameters if the default value for 'withDeleted' is not used`, async () => {
      // Arrange
      const spy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([expectedPost]));

      const expectedFilteredObject = { where: { isDeleted: false } };

      // Act
      await service.all(true);

      // Assert
      expect(spy).not.toBeCalledWith(expectedFilteredObject);
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return transformed array of ReturnPostDTO objects', async () => {
      // Arrange
      const spy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([post]));

      // Act
      const result = await service.all(true);

      // Assert
      expect(result.length).toEqual(1);
      expect(result[0]).toBeInstanceOf(ReturnPostDTO);

      jest.clearAllMocks();
    });
  });

  describe('allOwn()', () => {
    it(`should call usersRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const spy = jest.spyOn(usersRepo, 'findOne');

      // Act
      await service.allOwn(user);

      // Assert
      expect(spy).toBeCalledWith({ where: { username: user.username, isDeleted: false } });
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found user is undefined (does not exist)`, async () => {
      // Arrange
      jest.spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.allOwn(user)).rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsRepository find(), once with correct,
     filtering object if the default value for 'withDeleted' is used`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindSpy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      // Act
      await service.allOwn(user);

      // Assert
      expect(mockedPostRepositoryFindSpy)
        .toBeCalledWith({ where: { userCreator: mockedUserEntity, isDeleted: false } });
      expect(mockedPostRepositoryFindSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsRepository find(),
     once with no parameters if the default value for 'withDeleted' is not used`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindSpy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      // Act
      await service.allOwn(user, true);

      // Assert
      expect(mockedPostRepositoryFindSpy)
        .not.toBeCalledWith({ where: { userCreator: mockedUserEntity, isDeleted: false} });
      expect(mockedPostRepositoryFindSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return transformed array of ReturnPostCommentDTO objects', async () => {
      // Arrange
      const spy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([post]));

      // Act
      const result = await service.allOwn(user, true);

      // Assert
      expect(result.length).toEqual(1);
      expect(result[0]).toBeInstanceOf(ReturnPostDTO);
      expect(result[0]).toEqual(expectedPost);

      jest.clearAllMocks();
    });
  });

  describe('allOwnByOptions()', () => {
    it(`should call usersRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const spy = jest.spyOn(usersRepo, 'findOne');

      // Act
      await service.allOwnByOptions(user, options);

      // Assert
      expect(spy).toBeCalledWith({ where: { username: user.username, isDeleted: false } });
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found user is undefined (does not exist)`, async () => {
      // Arrange
      jest.spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.allOwnByOptions(user, options)).rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsRepository find(), once with correct,
     filtering object if the default value for 'withDeleted' is used`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindSpy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      // Act
      await service.allOwnByOptions(user, options);

      // Assert
      expect(mockedPostRepositoryFindSpy)
        .toBeCalledWith({ where: { title: options.title, userCreator: mockedUserEntity, isDeleted: false } });
      expect(mockedPostRepositoryFindSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsRepository find(),
     once with no parameters if the default value for 'withDeleted' is not used`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindSpy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([]));

      // Act
      await service.allOwnByOptions(user, options, true);

      // Assert
      expect(mockedPostRepositoryFindSpy).not.toBeCalledWith({
        where: {
          title: options.title,
          userCreator: mockedUserEntity,
          isDeleted: false,
        },
      });
      expect(mockedPostRepositoryFindSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return transformed array of ReturnPostCommentDTO objects', async () => {
      // Arrange
      const spy = jest
        .spyOn(postsRepo, 'find')
        .mockReturnValue(Promise.resolve([post]));

      // Act
      const result = await service.allOwnByOptions(user, options, true);

      // Assert
      expect(result.length).toEqual(1);
      expect(result[0]).toBeInstanceOf(ReturnPostDTO);
      expect(result[0]).toEqual(expectedPost);

      jest.clearAllMocks();
    });
  });

  describe('create()', () => {
    it(`should call usersRepository findOne(),
      once with correct parameters`, async () => {
      // Arrange
      const mockedPostalsRepositoryCreateSpy = jest
      .spyOn(postsRepo, 'create')
      .mockReturnValue(post);

      const spy = jest.spyOn(usersRepo, 'findOne');

      // Act
      await service.create(post, user);

      // Assert
      expect(spy).toBeCalledWith({ where: { username: user.username, isDeleted: false } });
      expect(spy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });
    it('should throw error if the found user does not exist', () => {
      // Arrange
      const mockedPostalsRepositoryCreateSpy = jest
      .spyOn(postsRepo, 'create')
      .mockReturnValue(post);

      jest.spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.create(post, user)).rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });
    it('should call postsRepository create() once with correct parameters', async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
          .spyOn(usersRepo, 'findOne')
          .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryCreateSpy = jest
          .spyOn(postsRepo, 'create')
          .mockReturnValue(post);

      // Act
      await service.create(post, user);

      // Assert
      expect(mockedPostRepositoryCreateSpy)
          .toBeCalledWith(post);
      expect(mockedPostRepositoryCreateSpy)
          .toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call postsRepository save() once with correct parameters', async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      const mockedUsersRepositoryFindOneSpy = jest
          .spyOn(usersRepo, 'findOne')
          .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryCreateSpy = jest
          .spyOn(postsRepo, 'create')
          .mockReturnValue(post);

      const mockedPostRepositorySaveSpy = jest
          .spyOn(postsRepo, 'save')
          .mockReturnValue(Promise.resolve(post));

      // Act
      await service.create(post, user);

      // Assert
      expect(mockedPostRepositorySaveSpy).toBeCalledWith(post);
      expect(mockedPostRepositorySaveSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return transformed ReturnPostCommentDTO object', async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      const mockedUsersRepositoryFindOneSpy = jest
          .spyOn(usersRepo, 'findOne')
          .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryCreateSpy = jest
          .spyOn(postsRepo, 'create')
          .mockReturnValue(post);

      const mockedPostRepositorySaveSpy = jest
          .spyOn(postsRepo, 'save')
          .mockReturnValue(Promise.resolve(post));

      // Act
      const result = await service.create(post, user);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostDTO);
      expect(result).toEqual(expectedPost);

      jest.clearAllMocks();
    });
  });

  describe('update()', () => {
  it(`should call postsRepository findOne(),
    once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      // Act
      await service.update(user, post.id, updatePost);

      // Assert
      expect(mockedPostsRepositoryFindOneSpy)
        .toBeCalledWith({ where: { id: post.id, isDeleted: false } });
      expect(mockedPostsRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

  it(`should throw error if the found post is undefined (does not exist)`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.update(user, post.id, updatePost))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

  it(`should throw error if the found post is 'Locked'`, async () => {
      // Arrange
      const mockedPost = new Posts();
      mockedPost.isLocked = true;
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      // Act & Assert
      expect(service.update(user, post.id, updatePost))
        .rejects.toThrowError(`Locked post!`);

      jest.clearAllMocks();
    });

  it(`should call postsRepository update(),
      if user role is 'Admin'`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositoryUpdateSpy = jest
        .spyOn(postsRepo, 'update');

      // Act
      await service.update(user, post.id, updatePost);

      // Assert
      expect(mockedPostsRepositoryUpdateSpy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
   });

  it(`should call postsRepository update(), if user role is 'Basic',
    but is the author of the comment`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const basicUserButAuthor: ShowUserDTO = {
        ...user,
        username: (await post.userCreator).username,
        roles: ['Basic'],
      };

      const mockedPostsRepositoryFindOneSpy = jest
      .spyOn(postsRepo, 'findOne')
      .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositoryUpdateSpy = jest
      .spyOn(postsRepo, 'update');

      // Act
      await service.update(basicUserButAuthor, post.id, updatePost);

      // Assert
      expect(mockedPostsRepositoryUpdateSpy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

  it(`should call postsRepository update(),
      once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositoryUpdateSpy = jest
        .spyOn(postsRepo, 'update');

      // Act
      await service.update(user, post.id, updatePost);

      // Assert
      expect(mockedPostsRepositoryUpdateSpy)
        .toBeCalledWith(post.id, updatePost);
      expect(mockedPostsRepositoryUpdateSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

  it(`should throw error if user role is 'Basic',
      and the author of the post is someone else`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const basicUserButAuthor: ShowUserDTO = {
        ...user,
        roles: ['Basic'],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositoryUpdateSpy = jest
        .spyOn(postsRepo, 'update');

      // Act & Assert
      expect(mockedPostsRepositoryUpdateSpy).not.toHaveBeenCalled();
      expect(service.update(basicUserButAuthor, post.id, updatePost))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

  it('should return transformed ReturnPostCommentDTO object', async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositoryUpdateSpy = jest
        .spyOn(postsRepo, 'update')
        .mockReturnValue(Promise.resolve(post));

      // Act
      const result = await service.update(user, post.id, updatePost);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostDTO);
      expect(result).toEqual(expectedPost);

      jest.clearAllMocks();
    });
  });

  describe('delete()', () => {
    it(`should call postsRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      // Act
      await service.delete(user, post.id);

      // Assert
      expect(mockedPostsRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found post does not exist`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.delete(user, post.id)).rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should throw error if the found post is 'Locked'`, async () => {
      // Arrange
      const mockedPost = new Posts();
      mockedPost.isLocked = true;
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      // Act & Assert
      expect(service.delete(user, post.id))
        .rejects.toThrowError(`Locked post!`);

      jest.clearAllMocks();
    });

    it(`should call postsRepository save(),
      if user role is 'Admin'`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save');

      // Act
      await service.delete(user, post.id);

      // Assert
      expect(mockedPostsRepositorySaveSpy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsRepository save(),
      if user role is 'Basic', but is the author of the comment`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const basicUserButAuthor: ShowUserDTO = {
        ...user,
        username: (await post.userCreator).username,
        roles: ['Basic'],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save');

      // Act
      await service.delete(basicUserButAuthor, post.id);

      // Assert
      expect(mockedPostsRepositorySaveSpy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should call postsRepository save(),
      once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save');

      // Act
      await service.delete(user, post.id);

      // Assert
      expect(mockedPostsRepositorySaveSpy)
        .toBeCalledWith({ ...post, isDeleted: true });
      expect(mockedPostsRepositorySaveSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if user role is 'Basic',
    and the author of the post is someone else`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const basicUserButAuthor: ShowUserDTO = {
        ...user,
        roles: ['Basic'],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save');

      // Act & Assert
      expect(mockedPostsRepositorySaveSpy).not.toHaveBeenCalled();
      expect(service.delete(basicUserButAuthor, post.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it('should return transformed ReturnPostCommentDTO object', async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      post.userCreator = Promise.resolve(mockedUserEntity);

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save')
        .mockReturnValue(Promise.resolve(post));

      // Act
      const result = await service.delete(user, post.id);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostDTO);
      expect(result).toEqual(expectedPost);

      jest.clearAllMocks();
    });
  });

  describe('lockUnlockPost()', () => {
    it(`should call postsRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedLockStatus = '1';

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      // Act
      await service.lockUnlockPost(post.id, mockedLockStatus);

      // Assert
      expect(mockedPostsRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found post does not exist`, async () => {
      // Arrange
      const mockedLockStatus = '1';

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.lockUnlockPost(post.id, mockedLockStatus))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should throw error if call with incorrect value for lockStatus,
    different from 1 or -1`, async () => {
      // Arrange
      const mockedLockStatus = '10';

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      // Act & Assert
      expect(service.lockUnlockPost(post.id, mockedLockStatus))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsRepository save(),
      once with correct parameters`, async () => {
      // Arrange
      const mockedLockStatus = '1';
      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save');

      // Act
      await service.lockUnlockPost(post.id, mockedLockStatus);

      // Assert
      expect(mockedPostsRepositorySaveSpy)
        .toBeCalledWith({...post, isLocked: true});
      expect(mockedPostsRepositorySaveSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should return transformed ReturnPostCommentDTO object', async () => {
      // Arrange
      const mockedLockStatus = '1';
      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save')
        .mockReturnValue(Promise.resolve(post));

      // Act
      const result = await service.lockUnlockPost(post.id, mockedLockStatus);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostDTO);
      expect(result).toEqual(expectedPost);

      jest.clearAllMocks();
    });
  });

  describe('likePost()', () => {
    it(`should call postsRepository findOne(),
     once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      post = {
        ...post,
        votes: [mockedUserEntity],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      // Act
      await service.likePost(user.id, post.id);

      // Assert
      expect(mockedPostsRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found post is undefined (does not exist)`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      post = {
        ...post,
        votes: [mockedUserEntity],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      // Act & Assert
      expect(service.likePost(user.id, post.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should throw error if the found post is 'Locked'`, async () => {
      // Arrange
      const mockedPost = new Posts();
      mockedPost.isLocked = true;
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      const mockedPostRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedPost));

      // Act & Assert
      expect(service.likePost(user.id, post.id))
        .rejects.toThrowError(`Locked post!`);

      jest.clearAllMocks();
    });

    it(`should call usersRepository findOne(),
      once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      post = {
        ...post,
        votes: [mockedUserEntity],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      // Act
      await service.likePost(user.id, post.id);

      // Assert
      expect(mockedUsersRepositoryFindOneSpy)
        .toBeCalledWith({ where: { id: user.id, isDeleted: false } });
      expect(mockedUsersRepositoryFindOneSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should throw error if the found user is undefined (does not exist)`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(undefined));

      post = {
        ...post,
        votes: [mockedUserEntity],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      // Act & Assert
      expect(service.likePost(user.id, post.id))
        .rejects.toThrow(ForumSystemError);

      jest.clearAllMocks();
    });

    it(`should call postsRepository save(),
      once with correct parameters`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();

      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      post = {
        ...post,
        votes: [mockedUserEntity],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save');

      // Act
      await service.likePost(user.id, post.id);

      // Assert
      expect(mockedPostsRepositorySaveSpy)
        .toBeCalledWith(post);
      expect(mockedPostsRepositorySaveSpy).toBeCalledTimes(1);

      jest.clearAllMocks();
    });

    it(`should return transformed ReturnPostpostDTO object`, async () => {
      // Arrange
      const mockedUserEntity = new UserEntity();
      const mockedUsersRepositoryFindOneSpy = jest
        .spyOn(usersRepo, 'findOne')
        .mockReturnValue(Promise.resolve(mockedUserEntity));

      post = {
        ...post,
        votes: [mockedUserEntity],
      };

      const mockedPostsRepositoryFindOneSpy = jest
        .spyOn(postsRepo, 'findOne')
        .mockReturnValue(Promise.resolve(post));

      const mockedPostsRepositorySaveSpy = jest
        .spyOn(postsRepo, 'save')
        .mockReturnValue(Promise.resolve(post));

      // Act
      const result = await service.likePost(user.id, post.id);

      // Assert
      expect(result).toBeInstanceOf(ReturnPostDTO);
      expect(result).toEqual(expectedPost);

      jest.clearAllMocks();
    });
  });
});
