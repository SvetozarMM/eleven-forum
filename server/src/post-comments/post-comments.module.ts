import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PostsComments } from '../data/entities/post-comments.entity';
import { PostCommentsService } from './post-comments.service';
import { PostCommentsController } from './post-comments.controller';
import { UserEntity } from '../data/entities/user.entity';
import { Posts } from '../data/entities/postal.entity';
import { UserActivityService } from '../users/user-activity.service';
import { UserActivity } from '../data/entities/activity.entity';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([PostsComments, UserEntity, Posts, UserActivity]),
    UsersModule,
  ],
  providers: [PostCommentsService, UserActivityService],
  controllers: [PostCommentsController],
})
export class PostCommentsModule {}
