import { CommentDTO } from './comment.dto';
import { UserDTO } from './user.dto';

export interface Post {
    id: string;
    title: string;
    content: string;
    createdOn: Date;
    isLocked: boolean;
    comments: CommentDTO[];
    userCreator: UserDTO;
    votes: string[];
}
