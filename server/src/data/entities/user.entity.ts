import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToMany, JoinTable, OneToOne, JoinColumn } from 'typeorm';
import { PostsComments } from './post-comments.entity';
import { Role } from './role.entity';
import { Posts } from './postal.entity';
import { BanStatus } from './banstatus.entity';
import { UserActivity } from './activity.entity';

@Entity('user')export class UserEntity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({ type: 'nvarchar', nullable: false, unique: true })
    username: string;

    @Column({ type: 'nvarchar', nullable: false })
    password: string;

    @Column({ type: 'nvarchar', nullable: false })
    email: string;

    @Column({ nullable: true })
    avatar: string;

    @Column({ nullable: false, default: false })
    isDeleted: boolean;

    @OneToMany(type => PostsComments, comment => comment.userCreator)
    comments: Promise<PostsComments[]>;

    @OneToMany(type => Posts, posts => posts.userCreator)
    posts: Promise<Posts[]>;

    @ManyToMany(type => Role, { eager: true })
    @JoinTable()
    roles: Role[];

    @OneToOne(type => BanStatus, { eager: true })
    @JoinColumn()
    banstatus: BanStatus;

    @ManyToMany(type => Posts, post => post.votes, {eager: false})
    @JoinTable()
    likedPosts: Promise<Posts[]>;

    @ManyToMany(type => PostsComments, comment => comment.votes, {eager: false})
    @JoinTable()
    likedComment: Promise<PostsComments[]>;

    @OneToMany(type => UserActivity, activity => activity.user)
    activity: UserActivity[];

    @ManyToMany(type => UserEntity, user => user.friends)
    @JoinTable()
    friends: UserEntity[];
}
