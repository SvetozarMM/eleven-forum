import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { TestingModule, Test } from '@nestjs/testing';
import { CreateUserDTO } from './models/create-user.dto';
import { ShowUserDTO } from './models/show-user.dto';
import { FindUserDTO } from './models/find-user-dto';
import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import { UserRole } from './enums/user-role.enum';
import { UpdateUserBanStatusDTO } from './models/update-user-banstatus.dto';
import { UserFriendsDTO } from './models/user-friends.dto';
import { UserEntity } from '../data/entities/user.entity';

describe('TodosController', () => {
  let controller: UsersController;
  let usersService: Partial<UsersService>;
  let user: CreateUserDTO;
  let expectedUser: ShowUserDTO;
  let findUser: FindUserDTO;
  let userRoles: UpdateUserRolesDTO;
  let userBanStatus: UpdateUserBanStatusDTO;
  let friend: UserFriendsDTO;

  beforeEach(async () => {
    usersService = {
      allUsers() { return null; },
      findByName() { return null; },
      validatePassword() { return null; },
      create() { return null; },
      updateUserRoles() { return null; },
      updateUserBanStatus() { return null; },
      delete() { return null; },
      addFriend() { return null; },
      removeFriend() { return null; },
    };

    user = {
      username: 'TestName',
      password: 'userPassword1',
      email: 'testname@test.com',
    };

    expectedUser = {
      id: 'qwerty12345',
      username: 'TestName',
      email: 'testname@test.com',
      avatar: 'test.jpeg',
      roles: ['Basic'],
      banstatus: { id: 1, isBanned: false, description: ''},
    };

    findUser = {
      id: 'qwerty12345',
      username: 'TestName',
    };

    userRoles = {
      roles: [UserRole.Basic],
    };

    userBanStatus = {
      description: '',
      isBanned: false,
    };

    friend = {
      id: 1,
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        {
          provide: UsersService,
          useValue: usersService,
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(controller).toBeDefined();
  });
  describe('returnUsers()', () => {
  it('should call usersService allUsers(), once', async () => {
      // Arrange
      const spy = jest.spyOn(usersService, 'allUsers');

      // Act;
      await controller.returnUsers();

      // Asser
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

  it('should return the result from usersService allUsers()', async () => {
      // Arrange
      const spy = jest
        .spyOn(usersService, 'allUsers')
        .mockReturnValue(Promise.resolve([expectedUser]));

      // Act
      const response = await controller.returnUsers();

      // Assert
      expect(response).toEqual([expectedUser]);

      jest.clearAllMocks();
    });
  });

  describe('returnUserByName()', () => {
  it('should call usersService findByName(), once', async () => {
      // Arrange
      const spy = jest.spyOn(usersService, 'findByName');

      // Act;
      await controller.returnUserByName(findUser);

      // Asser
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

  it('should call usersService findByName() with correct parameters', async () => {
      // Arrange
      const spy = jest.spyOn(usersService, 'findByName');

      // Act
      await controller.returnUserByName(findUser);

      // Assert
      expect(spy).toHaveBeenCalledWith(findUser.username);

      jest.clearAllMocks();
    });

  it('should return the result from usersService findByName()', async () => {
      // Arrange
      const spy = jest
        .spyOn(usersService, 'findByName')
        .mockReturnValue(Promise.resolve(expectedUser));

      // Act
      const response = await controller.returnUserByName(findUser);

      // Assert
      expect(response).toEqual(expectedUser);

      jest.clearAllMocks();
    });
  });

  describe('addNewUser()', () => {
    it('should call usersService create(), once', async () => {
      // Arrange
      const spy = jest.spyOn(usersService, 'create');

      // Act
      await controller.addNewUser(user);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call usersService create() with correct parameters', async () => {
      // Arrange
      const userRoleToCreate = 'Basic';
      const spy = jest.spyOn(usersService, 'create');

      // Act
      await controller.addNewUser(user);

      // Assert
      expect(spy).toHaveBeenCalledWith(user, userRoleToCreate);

      jest.clearAllMocks();
    });

    it('should return correct user object', async () => {
      // Arrange
      const spy = jest
        .spyOn(usersService, 'create')
        .mockReturnValue(Promise.resolve(expectedUser));

      // Act
      const response = await controller.addNewUser(user);

      // Assert
      expect(response).toEqual(expectedUser);
    });
  });

  describe('updateUserRoles()', () => {
    it('should call usersService updateUserRoles(), once', async () => {
      // Arrange
      const userID: string = 'qwerty12345';
      const spy = jest.spyOn(usersService, 'updateUserRoles');

      // Act
      await controller.updateUserRoles(userID, userRoles);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call usersService updateUserRoles() with correct parameters', async () => {
      // Arrange
      const userID: string = 'qwerty12345';
      const spy = jest.spyOn(usersService, 'updateUserRoles');

      // Act
      await controller.updateUserRoles(userID, userRoles);

      // Assert
      expect(spy).toHaveBeenCalledWith(userID, userRoles);

      jest.clearAllMocks();
    });

    it('should return correct user object', async () => {
      // Arrange
      const userID: string = 'qwerty12345';
      const spy = jest
        .spyOn(usersService, 'updateUserRoles')
        .mockReturnValue(Promise.resolve(expectedUser));

      // Act
      const response = await controller.updateUserRoles(userID, userRoles);

      // Assert
      expect(response).toEqual(expectedUser);
    });
  });

  describe('updateUserBanStatus()', () => {
    it('should call usersService updateUserBanStatus(), once', async () => {
      // Arrange
      const userID: string = 'qwerty12345';
      const spy = jest.spyOn(usersService, 'updateUserBanStatus');

      // Act
      await controller.updateUserBanStatus(userID, userBanStatus);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call usersService updateUserBanStatus() with correct parameters', async () => {
      // Arrange
      const userID: string = 'qwerty12345';
      const spy = jest.spyOn(usersService, 'updateUserBanStatus');

      // Act
      await controller.updateUserBanStatus(userID, userBanStatus);

      // Assert
      expect(spy).toHaveBeenCalledWith(userID, userBanStatus);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const userID: string = 'qwerty12345';
      const spy = jest
        .spyOn(usersService, 'updateUserBanStatus')
        .mockReturnValue(Promise.resolve(expectedUser));

      // Act
      const response = await controller.updateUserBanStatus(userID, userBanStatus);

      // Assert
      expect(response).toEqual({ message: `Status updated!` });
    });
  });

  describe('delete()', () => {
    it('should call usersService delete(), once', async () => {
      // Arrange
      const spy = jest.spyOn(usersService, 'delete');

      // Act
      await controller.delete(findUser);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call usersService delete() with correct parameters', async () => {
      // Arrange
      const spy = jest.spyOn(usersService, 'delete');

      // Act
      await controller.delete(findUser);

      // Assert
      expect(spy).toHaveBeenCalledWith(findUser.username);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const spy = jest
        .spyOn(usersService, 'delete')
        .mockReturnValue(Promise.resolve(expectedUser));

      // Act
      const response = await controller.delete(findUser);

      // Assert
      expect(response).toEqual({ message: `User deleted!` });
    });
  });

  describe('addFriend()', () => {
    it('should call usersService addFriend(), once', async () => {
      // Arrange
      const mockedId: number = 1;
      const mockedUser: UserEntity = new UserEntity();
      const spy = jest.spyOn(usersService, 'addFriend');

      // Act
      await controller.addFriend(mockedId, friend, mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call usersService addFriend() with correct parameters', async () => {
      // Arrange
      const mockedId: number = 1;
      const mockedUser: UserEntity = new UserEntity();
      const spy = jest.spyOn(usersService, 'addFriend');

      // Act
      await controller.addFriend(mockedId, friend, mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledWith(mockedId, friend, mockedUser);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const mockedId: number = 1;
      const mockedUser: UserEntity = new UserEntity();
      const spy = jest
        .spyOn(usersService, 'addFriend')
        .mockReturnValue(Promise.resolve({ message: 'test'}));

      // Act
      const response = await controller.addFriend(mockedId, friend, mockedUser);

      // Assert
      expect(response).toEqual({ message: 'test' });
    });
  });

  describe('removeFriend()', () => {
    it('should call usersService removeFriend(), once', async () => {
      // Arrange
      const mockedId: number = 1;
      const mockedUser: UserEntity = new UserEntity();
      const spy = jest.spyOn(usersService, 'removeFriend');

      // Act
      await controller.removeFriend(mockedId, friend, mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);

      jest.clearAllMocks();
    });

    it('should call usersService removeFriend() with correct parameters', async () => {
      // Arrange
      const mockedId: number = 1;
      const mockedUser: UserEntity = new UserEntity();
      const spy = jest.spyOn(usersService, 'removeFriend');

      // Act
      await controller.removeFriend(mockedId, friend, mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledWith(mockedId, friend, mockedUser);

      jest.clearAllMocks();
    });

    it('should return correct message', async () => {
      // Arrange
      const mockedId: number = 1;
      const mockedUser: UserEntity = new UserEntity();
      const spy = jest
        .spyOn(usersService, 'removeFriend')
        .mockReturnValue(Promise.resolve({ message: 'test'}));

      // Act
      const response = await controller.removeFriend(mockedId, friend, mockedUser);

      // Assert
      expect(response).toEqual({ message: 'test' });
    });
  });
});
