import { BanstatusDTO } from './banstatus.dto';

export interface UserDTO {
    id: string;
    username: string;
    email: string;
    avatar: string;
    roles: string[];

    posts: string[];
    comments: string[];

    banstatus: BanstatusDTO;

    likedPosts: string[];
    likedComments: string[];

    friends: string[];
}
