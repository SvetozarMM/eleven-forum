import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-user-box-big',
  templateUrl: './user-box-big.component.html',
  styleUrls: ['./user-box-big.component.css']
})
export class UserBoxBigComponent {

  @Input() username: string;

}
