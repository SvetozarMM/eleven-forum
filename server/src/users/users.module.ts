import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserEntity } from '../data/entities/user.entity';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { Role } from '../data/entities/role.entity';
import { BanStatus } from '../data/entities/banstatus.entity';
import { MulterModule } from '@nestjs/platform-express';
import { extname } from 'path';
import { diskStorage } from 'multer';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { UserActivity } from '../data/entities/activity.entity';
import { UserActivityService } from './user-activity.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([UserEntity, Role, BanStatus, UserActivity]),
        MulterModule.register({
      fileFilter(_, file, cb) {
        const ext = extname(file.originalname);
        const allowedExtensions = ['.png', '.jpg', '.gif', '.jpeg'];

        if (!allowedExtensions.includes(ext)) {
          return cb(
            new ForumSystemError('Only images are allowed', 400),
            false,
          );
        }

        cb(null, true);
      },
      storage: diskStorage({
        destination: './avatars',
        filename: (_, file, cb) => {
          const rnd = Array.from({ length: 32 })
            .map(() => Math.round(Math.random() * 10))
            .join('');

          return cb(null, `${rnd}${extname(file.originalname)}`);
        },
      }),
    }),
    ],
    providers: [UsersService, UserActivityService],
    controllers: [UsersController],
    exports: [UsersService, UserActivityService],
})
export class UsersModule {}
