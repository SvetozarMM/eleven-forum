import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { UsersService } from '../../../core/services/users.service';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-activity-section',
  templateUrl: './activity-section.component.html',
  styleUrls: ['./activity-section.component.css']
})
export class ActivitySectionComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;
  activity: any = [];


  constructor(
    private readonly userService: UsersService,
    private readonly authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    ),
    this.userService.getUserActivity(this.user)
        .subscribe({
          next: data => {
            this.activity = data;
          }
        });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

}

