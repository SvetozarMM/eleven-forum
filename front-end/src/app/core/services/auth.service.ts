import { Router } from '@angular/router';
import { StorageService } from './storage.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserDTO } from '../../models/user.dto';
import { UserLoginDTO } from '../../models/user-login.dto';
import { UserRegisterDTO } from '../../models/user-register.dto';


@Injectable()
export class AuthService {

  private readonly helper = new JwtHelperService();

  private readonly isLoggedInSubject$ = new BehaviorSubject<boolean>(this.isUserLoggedIn());
  private readonly loggedUserSubject$ = new BehaviorSubject<UserDTO>(this.loggedUser());

  constructor(
    private readonly client: HttpClient,
    private readonly storage: StorageService,
    private readonly router: Router,
  ) { }

  get isLoggedIn$(): Observable<boolean> {
    return this.isLoggedInSubject$.asObservable();
  }

  get loggedUser$(): Observable<UserDTO> {
    return this.loggedUserSubject$.asObservable();
  }

  emitUserData(user: UserDTO): void {
    this.loggedUserSubject$.next(user);
  }


  login(user: UserLoginDTO) {
    return this.client.post<{ token: string }>(`http://localhost:3000/auth`, user)
      .pipe(
        tap(({ token }) => {
          try {
            const loggedUser = this.helper.decodeToken(token);
            this.storage.save('token', token);

            this.isLoggedInSubject$.next(true);
            this.loggedUserSubject$.next(loggedUser);
          } catch (error) {
          }
        }),
      );
  }

  logout() {
    this.storage.save('token', '');
    this.isLoggedInSubject$.next(false);
    this.loggedUserSubject$.next(null);

    this.router.navigate(['home']);
  }

  register(user: UserRegisterDTO): Observable<UserRegisterDTO> {
    return this.client.post<UserRegisterDTO>(`http://localhost:3000/users`, user);
  }

  private isUserLoggedIn(): boolean {
    return !!this.storage.read('token');
  }

  private loggedUser(): UserDTO {
    try {
      return this.helper.decodeToken(this.storage.read('token'));
    } catch (error) {
      this.isLoggedInSubject$.next(false);

      return null;
    }
  }
}
