import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Post } from '../../../models/post.model';
import { PostsService } from '../../posts.service';
import { CommentDTO } from '../../../models/comment.dto';
import { AuthService } from '../../../core/services/auth.service';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { CommentsService } from '../../../core/services/comments.service';
import { AddCommentService } from '../../../core/services/add-comment.service';
import { NotificatorService } from '../../../core/services/notificator.service';

@Component({
  selector: 'app-post-box-big',
  templateUrl: './post-box-big.component.html',
  styleUrls: ['./post-box-big.component.css']
})
export class PostBoxBigComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;

  @Input()
  post: Post;
  commentBoxloaded = false;
  postComments: CommentDTO[];
  isInEditMode = false;
  isCreateCommentBoxOpen: boolean;

  @Output()
  eventDelete: EventEmitter<Post> = new EventEmitter<Post>();
  @Output()
  eventLike: EventEmitter<Post> = new EventEmitter<Post>();
  @Output()
  eventUpdate: EventEmitter<Post> = new EventEmitter<Post>();
  @Output()
  eventCreate: EventEmitter<Post> = new EventEmitter<Post>();
  @Output()
  eventLockUnlock: EventEmitter<Post> = new EventEmitter<Post>();


  constructor(
    private readonly postsService: PostsService,
    private readonly authService: AuthService,
    private readonly commentService: CommentsService,
    private readonly addService: AddCommentService,
    private readonly notificator: NotificatorService,
  ) {}

  ngOnInit() {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.addService.currentIsCreatedCommentBoxOpen
      .subscribe(value => this.isCreateCommentBoxOpen = value);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  loadComments(postId: string): void {
    this.postsService
      .getComments(postId)
      .subscribe({
        next: (data: CommentDTO[]) => {
          this.postComments = data;
        },
        error: e => console.log(e)
      });
  }

  showHideCommentBox(postId: string) {
    this.loadComments(postId);
    this.commentBoxloaded = !this.commentBoxloaded;
  }

  showCreateCommentBox(postId: string) {
    this.addService.show();
    this.loadComments(postId);
    this.commentBoxloaded = true;
  }

  showHideEditorMode() {
    this.isInEditMode = !this.isInEditMode;
  }

  onLikePost(post: Post): void {
    this.eventLike.emit(post);
  }

  onDeletePost(post: Post): void {
    this.eventDelete.emit(post);
  }

  onLockUnlockPost(post: Post): void {
    this.eventLockUnlock.emit(post);
  }

  onUpdatePost(post: Post): void {
    this.eventUpdate.emit(post);
  }

  likeComment($event): void {
    if (!$event.votes.includes(this.user.username)) {
      this.commentService.likeComment($event)
        .subscribe({
          next: data => {
            const index = this.postComments.indexOf($event);
            this.postComments[index] = data;
          },
          error: e => console.log(e)
        });
    }
  }

  deleteComment($event): void {
    this.commentService.deleteComment($event.id)
      .subscribe({
        next: data => {
          this.postComments = this.postComments.filter(comment => comment.id !== $event.id);
        },
        error: e => console.log(e)
      });
  }

  updateComment($event): void {
    this.commentService.updateComment($event)
        .subscribe({
          next: data => {
            const index = this.postComments.indexOf($event);
            this.postComments[index] = data;
          },
          error: e => console.log(e)
        });
  }

  createComment($event): void {
    this.commentService.createComment($event, this.post.id)
      .subscribe({
        next: data => {
          this.postComments.push(data);
        },
        error: e => console.log(e)
      });
  }
}
