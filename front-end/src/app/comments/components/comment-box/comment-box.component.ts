import { Component, Input, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { CommentDTO } from '../../../models/comment.dto';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-comment-box',
  templateUrl: './comment-box.component.html',
  styleUrls: ['./comment-box.component.css']
})
export class CommentBoxComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  public user: UserDTO;

  @Input()
  comment: CommentDTO;

  isInEditMode = false;

  @Output()
  eventDeleteComment: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();
  @Output()
  eventLikeComment: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();
  @Output()
  eventUpdateComment: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();
  @Output()
  eventCreateComment: EventEmitter<CommentDTO> = new EventEmitter<CommentDTO>();

  constructor(
    private readonly authService: AuthService,
  ) {}

  public ngOnInit() {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  public ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  showHideEditorMode() {
    this.isInEditMode = !this.isInEditMode;
  }

  onLikeComment(comment: CommentDTO): void {
    this.eventLikeComment.emit(comment);
  }

  onDeleteComment(comment: CommentDTO): void {
    this.eventDeleteComment.emit(comment);
  }

  onUpdateComment(comment: CommentDTO): void {
    this.eventUpdateComment.emit(comment);
  }

}
