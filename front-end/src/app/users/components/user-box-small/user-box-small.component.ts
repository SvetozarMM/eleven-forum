import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-user-box-small',
  templateUrl: './user-box-small.component.html',
  styleUrls: ['./user-box-small.component.css']
})
export class UserBoxSmallComponent {

  @Input() username: string;

}
