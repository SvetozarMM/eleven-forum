import { Component, OnInit, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { UserDTO } from '../../../models/user.dto';
import { UsersService } from '../../../core/services/users.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-friend-box',
  templateUrl: './friend-box.component.html',
  styleUrls: ['./friend-box.component.css']
})
export class FriendBoxComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;

  @Input()
  friend: UserDTO;

  @Output()
  eventFollowUser: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();

  activity: any[];

  isActiveInfo = false;
  isActiveActivity = false;

  constructor(
    private readonly userService: UsersService,
    private readonly authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  onInfo() {
    this.isActiveInfo = !this.isActiveInfo;
  }
  onActivity(user: UserDTO) {
    this.isActiveActivity = !this.isActiveActivity;
    this.userService.getUserActivity(user)
      .subscribe({
        next: data => {
          this.activity = data;
        }
      });
  }
  onFollow(user) {
    this.eventFollowUser.emit(user);
  }

}
