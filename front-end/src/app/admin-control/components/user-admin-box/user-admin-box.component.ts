import { Component, OnInit, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { AuthService } from '../../../core/services/auth.service';
import { UsersService } from '../../../core/services/users.service';

@Component({
  selector: 'app-user-admin-box',
  templateUrl: './user-admin-box.component.html',
  styleUrls: ['./user-admin-box.component.css']
})
export class UserAdminBoxComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  userAuthenticate: UserDTO;
  isActiveInfo = false;
  isActiveActivity = false;
  isActiveRoles = false;
  isActiveBanstatus = false;
  isActiveDelete = false;

  activity: any = [];

  @Input()
  user: UserDTO;

  @Output()
  eventDeleteUser: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();
  @Output()
  eventUpdateRolesUser: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();
  @Output()
  eventUpdateBanstatusUser: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();
  @Output()
  eventActivityUser: EventEmitter<UserDTO> = new EventEmitter<UserDTO>();

  constructor(
    private readonly authService: AuthService,
    private readonly userService: UsersService,
  ) { }

  ngOnInit(): void {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.userAuthenticate = user,
    );
  }
  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  onInfo() {
    this.isActiveInfo = !this.isActiveInfo;
  }
  showActivityBox() {
    this.isActiveActivity = !this.isActiveActivity;
  }
  onActivity(user: UserDTO) {
    this.isActiveActivity = !this.isActiveActivity;
    this.userService.getUserActivity(user)
      .subscribe({
        next: data => {
          this.activity = data;
        }
      });
  }

  showRolesBox() {
    this.isActiveRoles = !this.isActiveRoles;
  }
  onAdminRole(user: UserDTO) {
    user.roles = ['Admin'];
    this.eventUpdateRolesUser.emit(user);
  }
  onBasicRole(user: UserDTO) {
    user.roles = ['Basic'];
    this.eventUpdateRolesUser.emit(user);
  }

  showBanstatusBox() {
    this.isActiveBanstatus = !this.isActiveBanstatus;
  }
  onBanUser(user: UserDTO) {
    user.banstatus.isBanned = true;
    this.eventUpdateBanstatusUser.emit(user);
  }
  onPermitUser(user: UserDTO) {
    user.banstatus.isBanned = false;
    this.eventUpdateBanstatusUser.emit(user);
  }

  showDeleteBox() {
    this.isActiveDelete = !this.isActiveDelete;
  }
  onDelete(user: UserDTO) {
    this.eventDeleteUser.emit(user);
  }

}
