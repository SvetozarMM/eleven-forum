import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserLoginDTO } from '../users/models/user-login-dto';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { UsersService } from '../users/users.service';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { ConfigService } from '@nestjs/config';


@Injectable()
export class AuthService {
    private readonly blacklist: string[] = [];

    public constructor(
        private readonly userService: UsersService,
        private readonly jwtService: JwtService,
        private readonly configService: ConfigService,
    ) {}

    public async login(user: UserLoginDTO): Promise<any> {
        const foundUser: ShowUserDTO = await this.userService.findByName(user.username);

        if (!foundUser) {
            throw new ForumSystemError('No such user', 400);
        }

        if (!(await this.userService.validatePassword(user))) {
            throw new ForumSystemError('Invalid password!', 400);
        }

        const payload: ShowUserDTO = { ...foundUser };
        const expTime: number = +(this.configService.get('JWT_EXPIRE_TIME'));

        return {
            token: await this.jwtService.signAsync(payload),
            expiresIn: expTime,
        };
    }

    public blacklistToken(token: string): void {
        this.blacklist.push(token);
      }

      public isTokenBlacklisted(token: string): boolean {
        return this.blacklist.includes(token);
      }
}
