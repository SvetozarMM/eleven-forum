import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, OneToMany, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { PostsComments } from './post-comments.entity';
import { UserEntity } from './user.entity';

@Entity('postals')
export class Posts {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('nvarchar', {length: 100})
    title: string;

    @Column('nvarchar')
    content: string;

    @CreateDateColumn()
    createdOn: Date;

    @Column({type: 'boolean', default: false})
    isLocked: boolean;

    @Column({type: 'boolean', default: false})
    isDeleted: boolean;

    @OneToMany(type => PostsComments, comment => comment.owner)
    comments: Promise<PostsComments[]>;

    @ManyToOne(type => UserEntity, user => user.posts, {eager: true})
    userCreator: UserEntity;

    @ManyToMany(type => UserEntity, user => user.likedPosts, {eager: true, cascade: true})
    @JoinTable()
    votes: UserEntity[];
}
