import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommentDTO } from '../../../models/comment.dto';
import { CommentsService } from '../../../core/services/comments.service';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-all-comments-box',
  templateUrl: './all-comments-box.component.html',
  styleUrls: ['./all-comments-box.component.css']
})
export class AllCommentsBoxComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;
  allComments: CommentDTO[];

  constructor(
    private readonly commentService: CommentsService,
    private readonly authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.commentService
      .getAllComments()
      .subscribe({
        next: (data: CommentDTO[]) => (this.allComments = data),
        error: e => console.log(e)
      });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  likeComment($event): void {
    if (!$event.votes.includes(this.user.username)) {
      this.commentService.likeComment($event)
        .subscribe({
          next: data => {
            const index = this.allComments.indexOf($event);
            this.allComments[index] = data;
          },
          error: e => console.log(e)
        });
    }
  }

  deleteComment($event): void {
    this.commentService.deleteComment($event.id)
      .subscribe({
        next: data => {
          this.allComments = this.allComments.filter(comment => comment.id !== $event.id);
        },
        error: e => console.log(e)
      });
  }

  updateComment($event): void {
    this.commentService.updateComment($event)
        .subscribe({
          next: data => {
            const index = this.allComments.indexOf($event);
            this.allComments[index] = data;
          },
          error: e => console.log(e)
        });
  }

}
