import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActivitySectionComponent } from './components/activity-section/activity-section.component';
import { UsersModule } from '../users/users.module';
import { ActivitySingleComponent } from './components/activity-single/activity-single.component';
import { ActivityPostsComponent } from './components/activity-posts/activity-posts.component';
import { ActivityCommentsComponent } from './components/activity-comments/activity-comments.component';
import { UserPageRouterModule } from '../user-page/user-page-router.module';
import { PostsModule } from '../posts/posts.module';
import { CommentsModule } from '../comments/comments.module';

@NgModule({
  declarations: [
    ActivitySectionComponent,
    ActivitySingleComponent,
    ActivityPostsComponent,
    ActivityCommentsComponent,
  ],
  imports: [
    CommonModule,
    UsersModule,
    UserPageRouterModule,
    PostsModule,
    CommentsModule,
  ]
})
export class ActivityModule { }
