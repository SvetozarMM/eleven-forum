import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validator } from 'class-validator';
import { AuthService } from '../../../core/services/auth.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  private readonly validator = new Validator();
  private loggedInSubscription: Subscription;
  public loggedIn: boolean;

  public userLoginCredentials: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  public ngOnInit() {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(
      loggedIn => this.loggedIn = loggedIn,
    );
  }

  public ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
  }

  login(username: string, password: string) {
    this.authService.login({
      username,
      password,
    })
    .subscribe(
      () => {
        this.notificator.success(`Login successful!`);
        this.router.navigate(['/session/posts']);
      },
      () => this.notificator.error(`Invalid username or password!`),
    );
  }
}
