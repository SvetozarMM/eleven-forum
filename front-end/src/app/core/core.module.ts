import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthService } from './services/auth.service';
import { NotificatorService } from './services/notificator.service';
import { StorageService } from './services/storage.service';
import { AuthGuard } from '../auth/auth.guard';

@NgModule({
  declarations: [],
  providers: [
    AuthService,
    NotificatorService,
    StorageService,
    AuthGuard,
  ],
  imports: [
    CommonModule,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('CoreModule already initiated!');
    }
  }
}
