import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserAdminBoxComponent } from './user-admin-box.component';

describe('UserAdminBoxComponent', () => {
  let component: UserAdminBoxComponent;
  let fixture: ComponentFixture<UserAdminBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserAdminBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserAdminBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
