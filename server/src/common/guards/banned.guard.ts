import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';

@Injectable()
export class BannedGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user = request.user;

    return user && user.banstatus.isBanned === false;
  }
}
