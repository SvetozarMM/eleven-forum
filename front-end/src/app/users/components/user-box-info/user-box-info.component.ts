import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { UserDTO } from 'src/app/models/user.dto';

@Component({
  selector: 'app-user-box-info',
  templateUrl: './user-box-info.component.html',
  styleUrls: ['./user-box-info.component.css']
})
export class UserBoxInfoComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  public user: UserDTO;

  constructor(
    private readonly authService: AuthService,
  ) {}

  public ngOnInit() {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  public ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

}
