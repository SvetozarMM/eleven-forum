import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddPostService {

  private isCreateBoxOpen = new BehaviorSubject(false);
  currentIsCreatedBoxOpen = this.isCreateBoxOpen.asObservable();

  constructor() { }

  show() {
    this.isCreateBoxOpen.next(true);
  }

  hide() {
    this.isCreateBoxOpen.next(false);
  }


}
