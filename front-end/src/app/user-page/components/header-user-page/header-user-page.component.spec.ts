import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderUserPageComponent } from './header-user-page.component';

describe('HeaderUserPageComponent', () => {
  let component: HeaderUserPageComponent;
  let fixture: ComponentFixture<HeaderUserPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderUserPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderUserPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
