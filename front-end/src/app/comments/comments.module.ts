import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommentBoxComponent } from './components/comment-box/comment-box.component';
import { UsersModule } from '../users/users.module';
import { AllCommentsBoxComponent } from './components/all-comments-box/all-comments-box.component';
import { CommentsService } from '../core/services/comments.service';
import { FormsModule } from '@angular/forms';
import { AddCommentComponent } from './components/add-comment/add-comment.component';

@NgModule({
  declarations: [
    CommentBoxComponent,
    AllCommentsBoxComponent,
    AddCommentComponent,
  ],
  imports: [
    CommonModule,
    UsersModule,
    FormsModule,
  ],
  providers: [
    CommentsService,
  ],
  exports: [
    CommentBoxComponent,
    AllCommentsBoxComponent,
    AddCommentComponent,
  ],
})
export class CommentsModule { }
