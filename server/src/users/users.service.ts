import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, In } from 'typeorm';
import { UserEntity } from '../data/entities/user.entity';
import { CreateUserDTO } from './models/create-user.dto';
import { UserRole } from './enums/user-role.enum';
import { Role } from '../data/entities/role.entity';
import * as bcrypt from 'bcrypt';
import { plainToClass } from 'class-transformer';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { ShowUserDTO } from './models/show-user.dto';
import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import { BanStatus } from '../data/entities/banstatus.entity';
import { UpdateUserBanStatusDTO } from './models/update-user-banstatus.dto';
import { UserLoginDTO } from './models/user-login-dto';
import { UpdateAvatarDTO } from './models/update-user-avatar.dto';
import { UserActivity } from '../data/entities/activity.entity';
import { UserActivityService } from './user-activity.service';
import { UserActivitiesType } from './enums/user-activity-enum';
import { type } from 'os';

@Injectable()
export class UsersService {
  public constructor(
    @InjectRepository(UserEntity) private readonly usersRepository: Repository<UserEntity>,
    @InjectRepository(Role) private readonly rolesRepository: Repository<Role>,
    @InjectRepository(BanStatus) private readonly banstatusRepository: Repository<BanStatus>,
    @InjectRepository(UserActivity) private readonly userActivityRepository: Repository<UserActivity>,
    private readonly userActivity: UserActivityService,
  ) {}

  async allUsers(withDeleted = false): Promise<ShowUserDTO[]> {
    const filteredUsers = withDeleted
      ? await this.usersRepository.find()
      : await this.usersRepository.find({
        where: { isDeleted: withDeleted },
        relations: ['friends'],
      });

    return plainToClass(ShowUserDTO, filteredUsers, {
      excludeExtraneousValues: true,
    });
  }

  async findByName(username: string): Promise<ShowUserDTO> {
    const userFound = await this.usersRepository.findOne({
      where: { username },
      relations: ['friends'],
    });

    if (userFound === undefined || userFound.isDeleted) {
      throw new ForumSystemError(`User with name ${username} does not exist!`, 404);
    }

    return plainToClass(ShowUserDTO, userFound, {
      excludeExtraneousValues: true,
    });
  }

  public async validatePassword(user: UserLoginDTO): Promise<boolean> {
    const userEntity: UserEntity = await this.usersRepository.findOne({
      username: user.username,
    });

    return await bcrypt.compare(user.password, userEntity.password);
  }

  async create(
    user: CreateUserDTO,
    ...roles: UserRole[]
    ): Promise<ShowUserDTO> {
    try {
      user.password = await bcrypt.hash(user.password, 10);
      const userEntity: UserEntity = this.usersRepository.create(user);

      const userRolesFiltering = roles.map(role => ({ name: role }));
      const roleEntities: Role[] = this.rolesRepository.create(userRolesFiltering);
      const savedRoleEntities: Role[] = await this.rolesRepository.save(roleEntities);
      userEntity.roles = savedRoleEntities;

      const banStatus: BanStatus = this.banstatusRepository.create();
      const savedBanStatus = await this.banstatusRepository.save(banStatus);
      userEntity.banstatus = savedBanStatus;

      userEntity.posts = Promise.resolve([]);
      userEntity.comments = Promise.resolve([]);
      userEntity.friends = [];
      userEntity.activity = [];

      const createdUser: UserEntity = await this.usersRepository.save(userEntity);

      return plainToClass(ShowUserDTO, createdUser, {
        excludeExtraneousValues: true,
      });
    } catch (error) {
      throw new ForumSystemError(`User already exists`, 400);
    }
  }

  async updateUser(
    id: string,
    avatar: UpdateAvatarDTO,
  ): Promise<ShowUserDTO> {
    const foundUser: UserEntity = await this.findById(id);
    const entityToUpdate: UserEntity = { ...foundUser, ...avatar };
    const savedUser: UserEntity = await this.usersRepository.save(entityToUpdate);

    return plainToClass(ShowUserDTO, savedUser, {
      excludeExtraneousValues: true,
    });
  }

  async updateUserRoles(
    id: string,
    updateUserRoles: UpdateUserRolesDTO,
  ): Promise<ShowUserDTO> {
    const foundUser: UserEntity = await this.findById(id);

    const roleEntities: Role[] = await this.rolesRepository.find({
      where: { name: In(updateUserRoles.roles) },
    });

    const entityToUpdate: UserEntity = { ...foundUser, roles: roleEntities };
    const savedUser: UserEntity = await this.usersRepository.save(entityToUpdate);

    return plainToClass(ShowUserDTO, savedUser, {
      excludeExtraneousValues: true,
    });
  }

  async updateUserBanStatus(
    userID: string,
    banstatus: UpdateUserBanStatusDTO,
  ): Promise<ShowUserDTO> {
    const foundUser: UserEntity = await this.findById(userID);

    await this.banstatusRepository.update(foundUser.banstatus.id, banstatus);

    const userBanStatusToUpdate: UserEntity = {
      ...foundUser,
      banstatus: {
        ...foundUser.banstatus,
        description: banstatus.description,
        isBanned: banstatus.isBanned,
      },
    };
    const savedUser: UserEntity = await this.usersRepository.save(userBanStatusToUpdate);

    return plainToClass(ShowUserDTO, savedUser, {
      excludeExtraneousValues: true,
    });
  }

  async delete(username: string): Promise<ShowUserDTO> {
    const foundUser = await this.usersRepository.findOne({
      where: { username, isDeleted: false },
    });

    if (foundUser === undefined) {
      throw new ForumSystemError(`User '${username}' does not exist!`, 404);
    }

    const savedUser = await this.usersRepository.save({...foundUser, isDeleted: true});

    return plainToClass(ShowUserDTO, savedUser, {
      excludeExtraneousValues: true,
    });
  }

  public async addFriend(friendId: string, user: UserEntity): Promise<ShowUserDTO> {

    try {
      await this.usersRepository.createQueryBuilder().relation('friends').of(user.id).add(friendId);
      this.userActivity.logFriendActivity(user, friendId, UserActivitiesType.Create);
    } catch (e) {
      // A
    }

    const savedUser = await this.usersRepository.findOne({
      where: { id: user.id },
      relations: ['friends'],
    });

    return plainToClass(ShowUserDTO, savedUser, {
      excludeExtraneousValues: true,
    });
  }

  public async getFriends(user: UserEntity): Promise<any> {
    const foundUser: UserEntity = await this.findById(user.id);
    const friends = foundUser.friends;
    return friends;
  }

  private async findById(id: string): Promise<UserEntity> {
    const userFound = await this.usersRepository.findOne({
      where: { id },
      relations: ['posts', 'comments', 'friends'],
    });

    if (userFound === undefined || userFound.isDeleted) {
      throw new ForumSystemError(`User with id ${id} does not exist!`, 404);
    }

    return userFound;
  }
}
