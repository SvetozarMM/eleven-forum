import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Posts } from '../data/entities/postal.entity';
import { ReturnPostDTO } from './models/postals/return-postals-dto';
import { UserEntity } from '../data/entities/user.entity';
import { plainToClass } from 'class-transformer';
import { UpdatePostDTO } from './models/postals/update-postals-dto';
import { ForumSystemError } from '../common/exceptions/forum-system.error';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { UserRole } from '../users/enums/user-role.enum';
import { UserActivityService } from '../users/user-activity.service';
import { UserActivitiesType } from '../users/enums/user-activity-enum';

@Injectable()
export class PostalsService {
  constructor(
    @InjectRepository(Posts)
    private readonly postsRepository: Repository<Posts>,
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
    private readonly activityService: UserActivityService,
  ) {}

  async all(withDeleted = false): Promise<ReturnPostDTO[]> {
    const filteredPosts: ReturnPostDTO[] = withDeleted
      ? await this.postsRepository.find()
      : await this.postsRepository.find({
          where: { isDeleted: withDeleted },
        });

    return plainToClass(ReturnPostDTO, filteredPosts, {
      excludeExtraneousValues: true,
    });
  }

  async byTitle(title: string, withDeleted = false): Promise<ReturnPostDTO[]> {
    const filteredPosts: ReturnPostDTO[] = withDeleted
      ? await this.postsRepository.find({ where: { title } })
      : await this.postsRepository.find({
          where: {
            title,
            isDeleted: withDeleted,
          },
        });

    return plainToClass(ReturnPostDTO, filteredPosts, {
      excludeExtraneousValues: true,
    });
  }

  async byId(id: string, withDeleted = false): Promise<ReturnPostDTO[]> {
    const filteredPosts: ReturnPostDTO[] = withDeleted
      ? await this.postsRepository.find({ where: { id } })
      : await this.postsRepository.find({
          where: {
            id,
            isDeleted: withDeleted,
          },
        });

    return plainToClass(ReturnPostDTO, filteredPosts, {
      excludeExtraneousValues: true,
    });
  }

  async allOwn(
    user: ShowUserDTO,
    withDeleted = false,
  ): Promise<ReturnPostDTO[]> {
    const foundUser = await this.usersRepository.findOne({
      where: { username: user.username, isDeleted: false },
    });

    if (foundUser === undefined) {
      throw new ForumSystemError(`User does not exist!`, 404);
    }

    const filteredPosts: ReturnPostDTO[] = withDeleted
      ? await this.postsRepository.find({
          where: { userCreator: foundUser },
        })
      : await this.postsRepository.find({
          where: {
            isDeleted: withDeleted,
            userCreator: foundUser,
          },
        });

    return plainToClass(ReturnPostDTO, filteredPosts, {
      excludeExtraneousValues: true,
    });
  }

  async create(
    post: Partial<Posts>,
    user: ShowUserDTO,
  ): Promise<ReturnPostDTO> {
    const userFound: UserEntity = await this.usersRepository.findOne({
      where: { username: user.username, isDeleted: false },
    });

    if (userFound === undefined) {
      throw new ForumSystemError(
        'User with such username does not exist!',
        400,
      );
    }

    const newPost = this.postsRepository.create(post);

    newPost.comments = Promise.resolve([]);
    newPost.userCreator = userFound;
    newPost.votes = [];

    const savedPost = await this.postsRepository.save(newPost);

    await this.activityService.logPostActivity(
      userFound,
      savedPost.id,
      UserActivitiesType.Posted,
    );

    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  async update(
    user: ShowUserDTO,
    postID: string,
    post: Partial<Posts>,
  ): Promise<ReturnPostDTO> {
    const foundPost = await this.findById(postID);
    if (foundPost.isLocked) {
      throw new ForumSystemError(`Locked post!`, 400);
    }
    const postCreatorName: string = (await foundPost.userCreator).username;

    const updatedPost =
      user.roles.includes(UserRole.Admin) || postCreatorName === user.username
        ? await this.postsRepository.save({
            ...foundPost,
            title: post.title,
            content: post.content,
          })
        : undefined;

    if (updatedPost === undefined) {
      throw new ForumSystemError(
        `You must be 'Administrator' to change this post!`,
        400,
      );
    }

    return plainToClass(ReturnPostDTO, updatedPost, {
      excludeExtraneousValues: true,
    });
  }

  async delete(user: ShowUserDTO, postID: string): Promise<ReturnPostDTO> {
    const foundPost = await this.findById(postID);
    if (foundPost.isLocked) {
      throw new ForumSystemError(`Locked post!`, 400);
    }
    const postCreatorName: string = (await foundPost.userCreator).username;

    const savedPost =
      user.roles.includes(UserRole.Admin) || postCreatorName === user.username
        ? await this.postsRepository.save({ ...foundPost, isDeleted: true })
        : undefined;

    if (savedPost === undefined) {
      throw new ForumSystemError(
        `You must be 'Administrator' to delete this post!`,
        400,
      );
    }

    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  async lockUnlockPost(
    postID: string,
    lockStatus: string,
  ): Promise<ReturnPostDTO> {
    const foundPost = await this.findById(postID);
    const status: boolean =
      lockStatus === '1' ? true : lockStatus === '-1' ? false : undefined;

    if (status === undefined) {
      throw new ForumSystemError(
        `Wrong request, '..lockstatus/ 1 (for lock) or -1 (for unlock)!`,
        400,
      );
    }

    const savedPost = await this.postsRepository.save({
      ...foundPost,
      isLocked: status,
    });

    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  async likePost(userID: string, postId: string): Promise<ReturnPostDTO> {
    const post: Posts = await this.findById(postId);
    if (post.isLocked) {
      throw new ForumSystemError(`Locked post!`, 400);
    }

    const user: UserEntity = await this.usersRepository.findOne({
      where: { id: userID, isDeleted: false },
    });

    if (user === undefined) {
      throw new ForumSystemError('Can not find user', 404);
    }

    const currentVotes = post.votes;
    post.votes = [...currentVotes, user];

    const savedPost = await this.postsRepository.save(post);

    return plainToClass(ReturnPostDTO, savedPost, {
      excludeExtraneousValues: true,
    });
  }

  private async findById(id: string): Promise<Posts> {
    const foundPost = await this.postsRepository.findOne({
      where: { id, isDeleted: false },
    });

    if (foundPost === undefined) {
      throw new ForumSystemError(`There is no post with id ${id}!`, 400);
    }

    return foundPost;
  }
}
