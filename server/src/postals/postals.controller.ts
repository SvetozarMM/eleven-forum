import { Controller, Get, Query, Post, Body, Put, Param, Delete, UseGuards, HttpCode, HttpStatus } from '@nestjs/common';
import { PostalsService } from './postals.service';
import { FindPostDTO } from './models/postals/find-postals-dto';
import { CreatePostDTO } from './models/postals/create-postals-dto';
import { UpdatePostDTO } from './models/postals/update-postals-dto';
import { ReturnPostDTO } from './models/postals/return-postals-dto';
import { ResponseMessageDTO } from './models/response-message.dto';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../common/decorators/user.decorator';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { AdminGuard } from '../common/guards/admin.guard';
import { BannedGuard } from '../common/guards/banned.guard';
import { PassportModule } from '@nestjs/passport';

@Controller('postals')
export class PostalsController {
  constructor(private readonly postsService: PostalsService) {}

  @Get('all')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  async allPosts(
    @Query() post: FindPostDTO,
  ): Promise<ReturnPostDTO[]> {
    if (Object.keys(post).length) {
      const options = {
        id: post.id,
        title: post.title,
      };
      if (options.id) {
        return await this.postsService.byId(options.id);
      }
      if (options.title) {
        return await this.postsService.byTitle(options.title);
      }
    }

    return await this.postsService.all();
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  async allOwnPosts(
    @User() user: ShowUserDTO,
  ): Promise<ReturnPostDTO[]> {
    return await this.postsService.allOwn(user);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async create(
    @Body() post: CreatePostDTO,
    @User() user: ShowUserDTO,
  ): Promise<ReturnPostDTO> {
    return await this.postsService.create(post, user);
  }

  @Put(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async update(
    @Body() post: UpdatePostDTO,
    @Param('id') id: string,
    @User() user: ShowUserDTO,
  ): Promise<ReturnPostDTO> {
    return await this.postsService.update(user, id, post);
  }

  @Put(':id/like')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async likePost(
    @Param('id') id: string,
    @User() user: ShowUserDTO,
  ): Promise<ReturnPostDTO> {
    return await this.postsService.likePost(user.id, id);
  }

  @Put(':id/lockstatus/:status')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async lockUnlockPost(
    @Param('id') postID: string,
    @Param('status') lockStatus: string,
  ): Promise<ReturnPostDTO> {
    return await this.postsService.lockUnlockPost(postID, lockStatus);
  }

  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async delete(
    @Param('id') id: string,
    @User() user: ShowUserDTO,
  ): Promise<ResponseMessageDTO> {
    await this.postsService.delete(user, id);

    return { message: `Post deleted!` };
  }
}
