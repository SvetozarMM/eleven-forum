import { Expose, Transform } from 'class-transformer';
import { BanStatus } from '../../data/entities/banstatus.entity';
import { UserEntity } from '../../data/entities/user.entity';

export class ShowUserDTO {
  @Expose()
  id: string;

  @Expose()
  username: string;

  @Expose()
  email: string;

  @Expose()
  avatar: string;

  @Expose()
  @Transform((_, obj) => obj.roles.map((x: any) => x.name))
  roles: string[];

  @Expose()
  @Transform((_, obj) => ({
    description: obj.banstatus.description,
    isBanned: obj.banstatus.isBanned,
  }))
  banstatus: BanStatus;

  @Expose()
  friends: UserEntity[];
}
