import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminControlUsersComponent } from './admin-control-users.component';
import { UsersService } from '../../../core/services/users.service';
import { NotificatorService } from 'src/app/core/services/notificator.service';
import { UsersModule } from '../../../users/users.module';
import { UserAdminBoxComponent } from '../user-admin-box/user-admin-box.component';
import { ActivityItemComponent } from '../activity-item/activity-item.component';
import { FormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { AuthService } from '../../../core/services/auth.service';

describe('AdminControlUsersComponent', () => {
  let component: AdminControlUsersComponent;
  let fixture: ComponentFixture<AdminControlUsersComponent>;
  let mockedUser: UserDTO;
  let usersService;
  let notificator;
  let authService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    mockedUser  = {
      id: '123qwe',
      username: 'user',
      email: 'userEmail',
      avatar: 'avatar',
      roles: ['Basic', 'Admin'],
      posts: [],
      comments: [],
      banstatus: {
        description: '',
        isBanned: false
      },
      likedPosts: [],
      likedComments: [],
      friends: [],
    };

    usersService = {
      getAllUsers() {
        return of([mockedUser]);
      },
      updateUserBanstatus() {},
      updateUserRoles() {},
      deleteUser() {}
    };

    notificator = {
      success() {},
      error() {},
      warning() {},
      info() {}
    };

    authService = {
      loggedUser$: of(mockedUser),
    };

    TestBed.configureTestingModule({
      imports: [
        UsersModule,
        FormsModule,
      ],
      declarations: [ AdminControlUsersComponent, UserAdminBoxComponent, ActivityItemComponent ],
      providers: [NotificatorService, UsersService, AuthService]
    })
    .overrideProvider(UsersService, { useValue: usersService })
    .overrideProvider(NotificatorService, { useValue: notificator })
    .overrideProvider(AuthService, { useValue: authService })
    .compileComponents()
    .then(() => {
      fixture = TestBed.createComponent(AdminControlUsersComponent);
      component = fixture.componentInstance;
    });
  }));

  beforeEach(() => {
      fixture = TestBed.createComponent(AdminControlUsersComponent);
      component = fixture.componentInstance;
      // fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should initialize correctly with the data', () => {
      // Arrange
      const users = [mockedUser];

      // Act
      component.ngOnInit();

      // Assert
      expect(component.users).toEqual(users);
    });

    it(`should call usersService "getAllUsers()" once`, () => {
      // Arrange
      const mockedUsers: UserDTO[] = [mockedUser];

      const spy = jest
        .spyOn(usersService, 'getAllUsers')
        .mockReturnValue(of(mockedUsers));

      // Act
      component.ngOnInit();

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });
  });
  describe('updateBanstatus()', () => {
    it(`should call usersService "updateUserBanstatus()" once`, () => {
      // Arrange
      component.users = [];
      const spy = jest
        .spyOn(usersService, 'updateUserBanstatus')
        .mockReturnValue(of(mockedUser));

      // Act
      component.updateBanstatus(mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it(`should call usersService "updateUserBanstatus()" with correct parameters`, () => {
      // Arrange
      component.users = [];
      const spy = jest
        .spyOn(usersService, 'updateUserBanstatus')
        .mockReturnValue(of(mockedUser));

      // Act
      component.updateBanstatus(mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledWith(mockedUser);
    });

    it(`should subscribe to the usersService "updateUserBanstatus()" observable,
     and save the data`, () => {
      // Arrange
      const updatedMockedUser = {
        ...mockedUser,
        banstatus: { description: '', isBanned: true },
      };

      component.users = [mockedUser];

      const spy = jest
        .spyOn(usersService, 'updateUserBanstatus')
        .mockReturnValue(of(updatedMockedUser));

      // Act
      component.updateBanstatus(mockedUser);

      // Assert
      expect(component.users).toEqual([updatedMockedUser]);
    });
  });

  describe('updateRoles()', () => {
    it(`should call usersService "updateUserRoles()" once`, () => {
      // Arrange
      component.users = [];
      const spy = jest
        .spyOn(usersService, 'updateUserRoles')
        .mockReturnValue(of(mockedUser));

      // Act
      component.updateRoles(mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it(`should call usersService "updateUserRoles()" with correct parameters`, () => {
      // Arrange
      component.users = [];
      const spy = jest
        .spyOn(usersService, 'updateUserRoles')
        .mockReturnValue(of(mockedUser));

      // Act
      component.updateRoles(mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledWith(mockedUser);
    });

    it(`should subscribe to the usersService "updateUserRoles()" observable,
     and save the data`, () => {
      // Arrange
      const updatedMockedUser = {
        ...mockedUser,
        roles: ['Basic'],
      };

      component.users = [mockedUser];

      const spy = jest
        .spyOn(usersService, 'updateUserRoles')
        .mockReturnValue(of(updatedMockedUser));

      // Act
      component.updateRoles(mockedUser);

      // Assert
      expect(component.users).toEqual([updatedMockedUser]);
    });
  });

  describe('deleteUser()', () => {
    it(`should call usersService "deleteUser()" once`, () => {
      // Arrange
      component.users = [];
      const spy = jest
        .spyOn(usersService, 'deleteUser')
        .mockReturnValue(of(mockedUser));

      // Act
      component.deleteUser(mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
    });

    it(`should call usersService "deleteUser()" with correct parameters`, () => {
      // Arrange
      component.users = [];
      const spy = jest
        .spyOn(usersService, 'deleteUser')
        .mockReturnValue(of(mockedUser));

      // Act
      component.deleteUser(mockedUser);

      // Assert
      expect(spy).toHaveBeenCalledWith(mockedUser);
    });

    it(`should subscribe to the usersService "deleteUser()" observable,
     and delete returned data`, () => {
      // Arrange
      component.users = [mockedUser];

      const spy = jest
        .spyOn(usersService, 'deleteUser')
        .mockReturnValue(of('user deleted'));

      // Act
      component.deleteUser(mockedUser);

      // Assert
      expect(component.users).toEqual([]);
    });

    it(`should subscribe to the usersService "deleteUser()" observable,
     and call notificator "success()" once when the response is successful`, () => {
      // Arrange
      component.users = [mockedUser];
      const spy = jest
        .spyOn(usersService, 'deleteUser')
        .mockReturnValue(of('User deleted'));

      const notificatorSpy = jest
        .spyOn(notificator, 'success');

      // Act
      component.deleteUser(mockedUser);

      // Assert
      expect(notificatorSpy).toHaveBeenCalled();
    });
  });
});
