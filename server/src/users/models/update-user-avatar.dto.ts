import { IsOptional, IsString } from 'class-validator';

export class UpdateAvatarDTO {
  @IsOptional()
  @IsString()
  public avatar: string;
}
