import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Post } from '../models/post.model';
import { CommentDTO } from '../models/comment.dto';
import { CreatePostDTO } from '../models/create-post.dto';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private readonly client: HttpClient) { }

  getAllPosts(): Observable<Post[]> {
    return this.client.get<Post[]>(`http://localhost:3000/postals/all`);
  }

  getPostById(id: string): Observable<Post> {
    return this.client.get<Post>(`http://localhost:3000/postals/all?id=${id}`);
  }

  getPostByTitle(title: string): Observable<Post[]> {
    return this.client.get<Post[]>(`http://localhost:3000/postals/all?title=${title}`);
  }

  getAllOwnPosts(): Observable<Post[]> {
    return this.client.get<Post[]>(`http://localhost:3000/postals`);
  }

  createPost(post: CreatePostDTO): Observable<Post> {
    return this.client.post<Post>(`http://localhost:3000/postals`, post);
  }

  updatePost(post: Post): Observable<Post> {
    return this.client.put<Post>(`http://localhost:3000/postals/${post.id}`, post);
  }

  deletePost(id: string): Observable<Post> {
    return this.client.delete<Post>(`http://localhost:3000/postals/${id}`);
  }

  likePost(post: Post): Observable<Post> {
    return this.client.put<Post>(`http://localhost:3000/postals/${post.id}/like`, post);
  }

  lockPost(post: Post): Observable<Post> {
    return this.client.put<Post>(`http://localhost:3000/postals/${post.id}/lockstatus/1`, post);
  }

  unlockPost(post: Post): Observable<Post> {
    return this.client.put<Post>(`http://localhost:3000/postals/${post.id}/lockstatus/-1`, post);
  }

  getComments(postId: string): Observable<CommentDTO[]> {
    return this.client.get<CommentDTO[]>(`http://localhost:3000/postals/${postId}/comments`);
  }

}
