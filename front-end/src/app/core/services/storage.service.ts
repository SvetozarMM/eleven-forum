import { Injectable } from '@angular/core';

@Injectable()
export class StorageService {

  constructor() { }

  save(key: string, value: any) {
    localStorage.setItem(key, String(value));
  }

  read(key: string) {
    const value =  localStorage.getItem(key);

    return value && value !== 'undefined'
      ? value
      : null;
  }

  delete(key: string) {
    localStorage.removeItem(key);
  }

  clear() {
    localStorage.clear();
  }

}
