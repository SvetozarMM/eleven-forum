import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn, ManyToOne, ManyToMany, JoinTable } from 'typeorm';
import { Posts } from './postal.entity';
import { UserEntity } from './user.entity';

@Entity('comments')
export class PostsComments {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column('text')
    content: string;

    @CreateDateColumn()
    createdOn: Date;

    @Column({type: 'boolean', default: false})
    isDeleted: boolean;

    @ManyToOne(type => Posts, post => post.comments)
    owner: Promise<Posts>;

    @ManyToOne(type => UserEntity, user => user.comments, {eager: true})
    userCreator: UserEntity;

    @ManyToMany(type => UserEntity, user => user.likedComment, {eager: true, cascade: true})
    @JoinTable()
    votes: UserEntity[];
}
