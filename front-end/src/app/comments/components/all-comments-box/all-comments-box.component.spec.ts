import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AllCommentsBoxComponent } from './all-comments-box.component';

describe('AllCommentsBoxComponent', () => {
  let component: AllCommentsBoxComponent;
  let fixture: ComponentFixture<AllCommentsBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllCommentsBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllCommentsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
