import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from 'src/app/models/user.dto';
import { CreateCommentDTO } from 'src/app/models/create-comment.dto';
import { AuthService } from '../../../core/services/auth.service';
import { AddCommentService } from '../../../core/services/add-comment.service';

@Component({
  selector: 'app-add-comment',
  templateUrl: './add-comment.component.html',
  styleUrls: ['./add-comment.component.css']
})
export class AddCommentComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;
  commentContent = '';
  comment: CreateCommentDTO = {
    content: '',
  };
  isCreateCommentBoxOpen: boolean;

  @Output()
  eventCreateComment: EventEmitter<CreateCommentDTO> = new EventEmitter<CreateCommentDTO>();

  constructor(
    private readonly authService: AuthService,
    private readonly addService: AddCommentService,
  ) { }

  ngOnInit() {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.addService.currentIsCreatedCommentBoxOpen
      .subscribe(value => this.isCreateCommentBoxOpen = value);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  onCreatePost() {
    this.comment.content = this.commentContent;
    this.eventCreateComment.emit(this.comment);

    this.comment.content = '';
    this.commentContent = '';
    this.hideCreateCommentBox();
  }

  hideCreateCommentBox() {
    this.addService.hide();
  }

}
