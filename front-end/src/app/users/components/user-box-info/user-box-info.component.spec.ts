import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserBoxInfoComponent } from './user-box-info.component';

describe('UserBoxInfoComponent', () => {
  let component: UserBoxInfoComponent;
  let fixture: ComponentFixture<UserBoxInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserBoxInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserBoxInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
