import { UserDTO } from './user.dto';

export interface CommentDTO {
    id: string;
    content: string;
    createdOn: Date;
    owner: string;
    userCreator: UserDTO;
    votes: string[];
}
