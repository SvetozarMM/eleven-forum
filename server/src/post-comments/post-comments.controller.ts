import { Controller, Get, Post, Body, Put, Param, Delete, UseGuards, HttpCode, HttpStatus } from '@nestjs/common';
import { PostCommentsService } from './post-comments.service';
import { CreatePostCommentDTO } from './model/create-post-comment-dto';
import { UpdatePostCommentDTO } from './model/update-post-comment-dto';
import { ReturnPostCommentDTO } from './model/return-post-comment-dto';
import { ResponseMessageDTO } from '../postals/models/response-message.dto';
import { AuthGuard } from '@nestjs/passport';
import { User } from '../common/decorators/user.decorator';
import { ShowUserDTO } from '../users/models/show-user.dto';
import { AdminGuard } from '../common/guards/admin.guard';
import { BannedGuard } from '../common/guards/banned.guard';
import { PassportModule } from '@nestjs/passport';

@Controller()
export class PostCommentsController {
  constructor(
    private readonly postsCommentService: PostCommentsService,
  ) {}

  @Get('comments/all')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  public async allComments(): Promise<ReturnPostCommentDTO[]> {

    return await this.postsCommentService.all();
  }

  @Get('postals/:postID/comments')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  public async postAllComments(
    @Param('postID') postID: string,
  ): Promise<ReturnPostCommentDTO[]> {

    return await this.postsCommentService.postAll(postID);
  }

  @Get('comments')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  public async allOwnComments(
    @User() user: ShowUserDTO,
  ): Promise<ReturnPostCommentDTO[]> {

    return await this.postsCommentService.allOwn(user);
  }

  @Post('postals/:postID/comment')
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async createComment(
    @Param('postID') postID: string,
    @Body() comment: CreatePostCommentDTO,
    @User() user: ShowUserDTO,
  ): Promise<ReturnPostCommentDTO> {
    return await this.postsCommentService.create(user, comment, postID);
  }

  @Put('comment/:commentID')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async updateComment(
      @Param('commentID') commentID: string,
      @Body() comment: UpdatePostCommentDTO,
      @User() user: ShowUserDTO,
    ): Promise<ReturnPostCommentDTO> {
    return await this.postsCommentService.update(user, commentID, comment);
  }

  @Put('comment/:commentID/like')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async likeComment(
    @Param('commentID') id: string,
    @User() user: ShowUserDTO,
  ): Promise<ReturnPostCommentDTO> {
    return await this.postsCommentService.likeComment(user.id, id);
  }

  @Delete('comment/:commentID')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async deleteComment(
    @Param('commentID') commentID: string,
    @User() user: ShowUserDTO,
  ): Promise<ResponseMessageDTO> {
    await this.postsCommentService.delete(user, commentID);

    return { message: `Comment deleted!` };
  }
}
