import { Component, OnInit, OnDestroy } from '@angular/core';
import { Post } from '../../../models/post.model';
import { PostsService } from '../../../posts/posts.service';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-activity-posts',
  templateUrl: './activity-posts.component.html',
  styleUrls: ['./activity-posts.component.css']
})
export class ActivityPostsComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;
  postsOwn: Post[] = [];

  constructor(
    private readonly postsService: PostsService,
    private readonly authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.postsService
      .getAllOwnPosts()
      .subscribe({
        next: (data: Post[]) => (this.postsOwn = data),
        error: e => console.log(e)
      });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  likePost($event): void {
    if (!$event.votes.includes(this.user.username) && !$event.isLocked) {
      this.postsService.likePost($event)
        .subscribe({
          next: data => {
            const index = this.postsOwn.indexOf($event);
            this.postsOwn[index] = data;
          },
          error: e => console.log(e)
        });
    }
  }

  deletePost($event): void {
    this.postsService.deletePost($event.id)
      .subscribe({
        next: data => {
          this.postsOwn = this.postsOwn.filter(post => post.id !== $event.id);
        },
        error: e => console.log(e)
      });
  }

  lockUnlockPost($event): void {
    if ($event.isLocked) {
      this.postsService.unlockPost($event)
        .subscribe({
          next: data => {
            const index = this.postsOwn.indexOf($event);
            this.postsOwn[index] = data;
          },
          error: e => console.log(e)
        });
    } else {
      this.postsService.lockPost($event)
        .subscribe({
          next: data => {
            const index = this.postsOwn.indexOf($event);
            this.postsOwn[index] = data;
          },
          error: e => console.log(e)
        });
    }
  }

  updatePost($event): void {
    this.postsService.updatePost($event)
        .subscribe({
          next: data => {
            const index = this.postsOwn.indexOf($event);
            this.postsOwn[index] = data;
          },
          error: e => console.log(e)
        });
  }

}
