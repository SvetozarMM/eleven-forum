import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminControlCommentsComponent } from './admin-control-comments.component';

describe('AdminControlCommentsComponent', () => {
  let component: AdminControlCommentsComponent;
  let fixture: ComponentFixture<AdminControlCommentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminControlCommentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminControlCommentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
