import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminControlSectionComponent } from './components/admin-control-section/admin-control-section.component';
import { AdminControlUsersComponent } from './components/admin-control-users/admin-control-users.component';
import { UsersModule } from '../users/users.module';
import { AdminControlPostsComponent } from './components/admin-control-posts/admin-control-posts.component';
import { PostsModule } from '../posts/posts.module';
import { CommentsModule } from '../comments/comments.module';
import { AdminControlCommentsComponent } from './components/admin-control-comments/admin-control-comments.component';
import { UsersService } from '../core/services/users.service';
import { UserPageRouterModule } from '../user-page/user-page-router.module';
import { FormsModule } from '@angular/forms';
import { UserAdminBoxComponent } from './components/user-admin-box/user-admin-box.component';
import { ActivityItemComponent } from './components/activity-item/activity-item.component';

@NgModule({
  declarations: [
    AdminControlSectionComponent,
    AdminControlUsersComponent,
    AdminControlPostsComponent,
    AdminControlCommentsComponent,
    UserAdminBoxComponent,
    ActivityItemComponent,
  ],
  providers: [
    UsersService,
  ],
  imports: [
    CommonModule,
    UsersModule,
    PostsModule,
    CommentsModule,
    UserPageRouterModule,
    FormsModule,
  ],
  exports: [
    ActivityItemComponent,
  ],
})
export class AdminControlModule { }
