import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserDTO } from '../../../models/user.dto';
import { UsersService } from '../../../core/services/users.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends.component.html',
  styleUrls: ['./friends.component.css']
})
export class FriendsComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;

  isSearchedResultOpen = false;
  searchedUser: UserDTO;
  friends: UserDTO[];

  username: string;

  constructor(
    private readonly userService: UsersService,
    private readonly authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.userService
      .getUserFriends()
      .subscribe({
        next: data => {
          this.friends = data;
        },
      });

    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  followUser($event) {
    if (!this.friends.includes($event)) {
      this.userService
      .addFriend($event)
      .subscribe({
        next: data => {
          this.friends.push(data);
        },
      });
    }
  }

  searchUser() {
    this.userService.getUserByName(this.username)
      .subscribe({
        next: data => {
          this.isSearchedResultOpen = true;
          this.friends = [];
          this.friends.push(data);
          this.username = '';
        },
      });
  }

  closeSearchedResultBox() {
    this.isSearchedResultOpen = false;
    this.userService
      .getUserFriends()
      .subscribe({
        next: data => {
          this.friends = data;
        },
      });
  }

}
