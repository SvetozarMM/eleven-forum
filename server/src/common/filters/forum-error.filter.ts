import { ExceptionFilter, Catch, ArgumentsHost } from '@nestjs/common';
import { Response } from 'express';
import { ForumSystemError } from '../exceptions/forum-system.error';

@Catch(ForumSystemError)
export class ForumSystemErrorFilter implements ExceptionFilter {
  public catch(exception: ForumSystemError, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();

    response.status(exception.code).json({
      status: exception.code,
      error: exception.message,
    });
  }
}
