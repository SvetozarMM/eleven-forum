import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActivityPostsComponent } from './activity-posts.component';

describe('ActivityPostsComponent', () => {
  let component: ActivityPostsComponent;
  let fixture: ComponentFixture<ActivityPostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActivityPostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActivityPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
