const postsSection = document.getElementById('posts');
const friendsSection = document.getElementById('friends');
const activitySection = document.getElementById('activity');
const aboutSection = document.getElementById('about');
const prefSection = document.getElementById('preferences');
const adminSection = document.getElementById('admin-control');

const goToPosts = () => {
    aboutSection.style.display = 'none';
    prefSection.style.display = 'none';
    adminSection.style.display = 'none';
    friendsSection.style.display = 'none';
    activitySection.style.display = 'none';
    postsSection.style.display = 'grid';
}

const goToFriends = () => {
    postsSection.style.display = 'none';
    activitySection.style.display = 'none';
    aboutSection.style.display = 'none';
    prefSection.style.display = 'none';
    adminSection.style.display = 'none';
    friendsSection.style.display = 'grid';
}

const goToActivity = () => {
    postsSection.style.display = 'none';
    friendsSection.style.display = 'none';
    aboutSection.style.display = 'none';
    prefSection.style.display = 'none';
    adminSection.style.display = 'none';
    activitySection.style.display = 'grid';
}

const goToAbout = () => {
    postsSection.style.display = 'none';
    friendsSection.style.display = 'none';
    activitySection.style.display = 'none';
    prefSection.style.display = 'none';
    adminSection.style.display = 'none';
    aboutSection.style.display = 'grid';
}

const goToPref = () => {
    postsSection.style.display = 'none';
    friendsSection.style.display = 'none';
    activitySection.style.display = 'none';
    adminSection.style.display = 'none';
    aboutSection.style.display = 'none';
    prefSection.style.display = 'grid';
}

const goToAdminControl = () => {
    postsSection.style.display = 'none';
    friendsSection.style.display = 'none';
    activitySection.style.display = 'none';
    prefSection.style.display = 'none';
    aboutSection.style.display = 'none';
    adminSection.style.display = 'grid';
}
