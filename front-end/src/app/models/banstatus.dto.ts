export interface BanstatusDTO {
    description: string;
    isBanned: boolean;
}
