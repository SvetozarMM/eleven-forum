import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { Validator } from 'class-validator';
import { AuthService } from '../../../core/services/auth.service';
import { NotificatorService } from '../../../core/services/notificator.service';
import { LoginComponent } from '../login/login.component';
import { Subscription } from 'rxjs';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy {

  private readonly validator = new Validator();
  private loggedInSubscription: Subscription;
  public loggedIn: boolean;

  public userRegisterCredentials: FormGroup = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(4), Validators.maxLength(12)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{5,}$/)]),
  });

  @Input()
  loginComponent: LoginComponent;

  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
  ) { }

  ngOnInit() {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(
      loggedIn => this.loggedIn = loggedIn,
    );
  }

  ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
  }

  register(username: string, email: string, password: string) {
    if (!username) {
      this.notificator.error(`Name should not be empty!`);
      return;
    }
    if (!password) {
      this.notificator.error(`Password should not be empty!`);
      return;
    }
    if (!this.validator.isEmail(email)) {
      this.notificator.error(`Email is not valid!`);
      return;
    }

    this.authService.register({
      username,
      email,
      password,
    })
      .subscribe(
        () => {
          this.notificator.success(`Registration successful!`);

          this.authService.login({
            username,
            password,
          })
          .subscribe(
            () => {
              this.notificator.success(`Login successful!`);
              this.router.navigate(['/session/posts']);
            },
            () => this.notificator.error(`Invalid username or password!`),
          );
        },
        () => this.notificator.error(`There was an error processing the request!`),
      );
  }

}
