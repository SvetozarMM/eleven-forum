import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AboutUsComponent } from './home-page/components/about-us/about-us.component';
import { TermsComponent } from './home-page/components/terms/terms.component';
import { LoginComponent } from './home-page/components/login/login.component';
import { RegisterComponent } from './home-page/components/register/register.component';

const routes: Routes = [
    { path: '', redirectTo: '/home/login', pathMatch: 'full' },
    { path: 'home',
        children: [
            {
                path: '',
                redirectTo: 'login',
                pathMatch: 'full',
            },
            {
                path: 'login',
                component: LoginComponent,
            },
            {
                path: 'register',
                component: RegisterComponent,
            },
            {
                path: 'about',
                component: AboutUsComponent,
            },
            {
                path: 'terms',
                component: TermsComponent,
            },
        ],
    },
    { path: 'session/posts', loadChildren: () => import('./user-page/user-page.module').then(m => m.UserPageModule) },
    { path: 'session/admin-control', loadChildren: () => import('./admin-control/admin-control.module').then(m => m.AdminControlModule) },
    { path: '404', component: NotFoundComponent },
    { path: '**', redirectTo: '/404', pathMatch: 'full'},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRouterModule {}
