import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserActivity } from '../data/entities/activity.entity';
import { Repository } from 'typeorm';
import { UserEntity } from '../data/entities/user.entity';
import { ShowUserActivityDto } from './models/show-user-activity.dto';
import { plainToClass } from 'class-transformer';
import { UserActivitiesType } from './enums/user-activity-enum';
import { EntityType } from './enums/entity.enum';

@Injectable()
export class UserActivityService {
  public constructor(
    @InjectRepository(UserActivity)
    private readonly userActivityRepository: Repository<UserActivity>,
    @InjectRepository(UserEntity)
    private readonly usersRepository: Repository<UserEntity>,
  ) {}

  async getUserActivity(userId: string): Promise<ShowUserActivityDto[]> {
    const loggedUser = await this.usersRepository.findOne(userId);
    const foundUserActivities = await this.userActivityRepository.find({
      where: { user: loggedUser },
      relations: ['user'],
    });

    const sortedUserActivity = foundUserActivities.sort((a, b) => {
        return (new Date(a.createdOn)) > (new Date(b.createdOn)) ? -1 : 1;
    });

    return sortedUserActivity.map(activity =>
      plainToClass(ShowUserActivityDto, activity, {
        excludeExtraneousValues: true,
      }),
    );
  }

  async logFriendActivity(
    user: UserEntity,
    friendId: string,
    action: UserActivitiesType,
  ): Promise<void> {
    const entity: EntityType = EntityType.User;

    const createdActivity = this.createActivity(user, friendId, entity, action);

    await this.userActivityRepository.save(createdActivity);
  }

  async logPostActivity(
    user: UserEntity,
    PostId: string,
    action: UserActivitiesType,
  ): Promise<void> {
    const entity: EntityType = EntityType.Post;

    const createdActivity = this.createActivity(user, PostId, entity, action);

    await this.userActivityRepository.save(createdActivity);
  }

  async logCommentActivity(
    user: UserEntity,
    CommentId: string,
    action: UserActivitiesType,
  ): Promise<void> {
    const entity: EntityType = EntityType.Comment;

    const createdActivity = this.createActivity(
      user,
      CommentId,
      entity,
      action,
    );

    await this.userActivityRepository.save(createdActivity);
  }

  private createActivity(
    user: UserEntity,
    entityId: string,
    entity: EntityType,
    action: UserActivitiesType,
  ) {
    return this.userActivityRepository.create({
      user,
      entityId,
      entity,
      userActivity: action,
    });
  }
}
