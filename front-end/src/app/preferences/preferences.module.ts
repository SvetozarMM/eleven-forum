import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrefSectionComponent } from './components/pref-section/pref-section.component';



@NgModule({
  declarations: [PrefSectionComponent],
  imports: [
    CommonModule
  ]
})
export class PreferencesModule { }
