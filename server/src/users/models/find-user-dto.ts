import { IsOptional, IsString } from 'class-validator';

export class FindUserDTO {
  @IsOptional()
  @IsString()
  id?: string;

  @IsOptional()
  @IsString()
  username?: string;
}
