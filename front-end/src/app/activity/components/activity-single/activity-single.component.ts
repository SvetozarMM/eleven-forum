import { Component, OnInit, Input } from '@angular/core';
import { UserDTO } from '../../../models/user.dto';
import { UsersService } from '../../../core/services/users.service';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-activity-single',
  templateUrl: './activity-single.component.html',
  styleUrls: ['./activity-single.component.css']
})
export class ActivitySingleComponent  {

  @Input()
  activityItem: any;

}
