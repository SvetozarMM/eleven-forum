import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserBoxBigComponent } from './components/user-box-big/user-box-big.component';
import { UserBoxSmallComponent } from './components/user-box-small/user-box-small.component';
import { UserBoxInfoComponent } from './components/user-box-info/user-box-info.component';

@NgModule({
  declarations: [
    UserBoxBigComponent,
    UserBoxSmallComponent,
    UserBoxInfoComponent,
  ],
  imports: [
    CommonModule
  ],
  providers: [],
  exports: [
    UserBoxBigComponent,
    UserBoxSmallComponent,
    UserBoxInfoComponent,
  ]
})
export class UsersModule { }
