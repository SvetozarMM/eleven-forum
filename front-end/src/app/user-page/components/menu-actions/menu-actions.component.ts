import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthService } from '../../../core/services/auth.service';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';

@Component({
  selector: 'app-menu-actions',
  templateUrl: './menu-actions.component.html',
  styleUrls: ['./menu-actions.component.css']
})
export class MenuActionsComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;

  constructor(
    private readonly authService: AuthService,
  ) { }

  public ngOnInit() {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
  }

  public ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  public logout() {
    this.authService.logout();
  }

}
