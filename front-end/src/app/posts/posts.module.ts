import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PostBoxSmallComponent } from './components/post-box-small/post-box-small.component';
import { PostBoxBigComponent } from './components/post-box-big/post-box-big.component';
import { UsersModule } from '../users/users.module';
import { CommentsModule } from '../comments/comments.module';
import { PostsSectionComponent } from './components/posts-section/posts-section.component';
import { PostsService } from './posts.service';
import { FormsModule } from '@angular/forms';
import { AddPostComponent } from './components/add-post/add-post.component';
import { CommentsService } from '../core/services/comments.service';

@NgModule({
  declarations: [
    PostBoxSmallComponent,
    PostBoxBigComponent,
    PostsSectionComponent,
    AddPostComponent,
  ],
  imports: [
    CommonModule,
    UsersModule,
    CommentsModule,
    FormsModule,
  ],
  providers: [
    PostsService,
    CommentsService,
  ],
  exports: [
    PostBoxSmallComponent,
    PostBoxBigComponent,
    PostsSectionComponent,
  ],
})
export class PostsModule { }
