import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostBoxSmallComponent } from './post-box-small.component';

describe('PostBoxSmallComponent', () => {
  let component: PostBoxSmallComponent;
  let fixture: ComponentFixture<PostBoxSmallComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostBoxSmallComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostBoxSmallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
