import { Expose } from 'class-transformer';

export class ResponseMessageDTO {
  @Expose()
  message: string;
}
