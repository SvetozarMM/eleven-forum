import { Component, OnInit, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../models/user.dto';
import { AuthService } from '../../../core/services/auth.service';
import { CreatePostDTO } from '../../../models/create-post.dto';
import { AddPostService } from '../../../core/services/add-post.service';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.css']
})
export class AddPostComponent implements OnInit, OnDestroy {

  private userSubscription: Subscription;
  user: UserDTO;
  postTitle = '';
  postContent = '';
  post: CreatePostDTO = {
    title: '',
    content: '',
  };
  isCreateBoxOpen: boolean;

  @Output()
  eventCreate: EventEmitter<CreatePostDTO> = new EventEmitter<CreatePostDTO>();

  constructor(
    private readonly authService: AuthService,
    private readonly addService: AddPostService,
  ) { }

  ngOnInit() {
    this.userSubscription = this.authService.loggedUser$.subscribe(
      user => this.user = user,
    );
    this.addService.currentIsCreatedBoxOpen
      .subscribe(value => this.isCreateBoxOpen = value);
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

  onCreatePost() {
    this.post.title = this.postTitle;
    this.post.content = this.postContent;
    this.eventCreate.emit(this.post);
    this.post.title = '';
    this.post.content = '';
    this.postTitle = '';
    this.postContent = '';
    this.hideCreateBox();
  }

  hideCreateBox() {
    this.addService.hide();
  }
}
