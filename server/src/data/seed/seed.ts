import { Role } from './../entities/role.entity';
import { createConnection, Repository, In } from 'typeorm';
import { UserRole } from '../../users/enums/user-role.enum';
import * as bcrypt from 'bcrypt';
import { UserEntity } from '../entities/user.entity';
import { BanStatus } from '../entities/banstatus.entity';
import { UserActivityService } from '../../users/user-activity.service';

// SENSITIVE DATA ALERT! - Normally the seed and the admin credentials should not be present in the public repository!
// Run: `npm run seed` to seed the database

const seedRoles = async (connection: any) => {
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);

  const roles: Role[] = await rolesRepo.find();
  if (roles.length) {
    console.log('The DB already has roles!');
    return;
  }

  const rolesSeeding: Promise<Role>[] = Object.keys(UserRole).map(
    async (roleName: string) => {
      const role: Role = rolesRepo.create({ name: roleName });
      return await rolesRepo.save(role);
    },
  );

  await Promise.all(rolesSeeding);
  console.log('Seeded roles successfully!');
};

const seedBanStatus = async (connection: any) => {
  const banstatusRepo: Repository<BanStatus> = connection.manager.getRepository(BanStatus);

  const banStatus: BanStatus = banstatusRepo.create({
    id: 1,
  });
  await banstatusRepo.save(banStatus);

  console.log('Seeded ban status successfully!');
};

const seedAdmin = async (connection: any) => {
  const userRepo: Repository<UserEntity> = connection.manager.getRepository(UserEntity);
  const rolesRepo: Repository<Role> = connection.manager.getRepository(Role);
  const banstatusRepo: Repository<BanStatus> = connection.manager.getRepository(BanStatus);

  const admin = await userRepo.findOne({
    where: {
      name: 'admin',
    },
  });

  if (admin) {
    console.log('The DB already has an admin!');
    return;
  }

  const roleNames: string[] = Object.keys(UserRole);
  const allUserRoles: Role[] = await rolesRepo.find({
    where: {
      name: In(roleNames),
    },
  });

  if (allUserRoles.length === 0) {
    console.log('The DB does not have any roles!');
    return;
  }

  const foundBanStatus = await banstatusRepo.findOne({
    where: { id: 1 },
  });

  if (!foundBanStatus) {
    console.log('The ban status does not have status with this id!');
    return;
  }

  const username = 'admin';
  const password = 'Aaa123';
  const email = 'admin@test.com';
  const hashedPassword = await bcrypt.hash(password, 10);

  const newAdmin: UserEntity = userRepo.create({
    username,
    password: hashedPassword,
    email,
    roles: allUserRoles,
    banstatus: foundBanStatus,
    posts: Promise.resolve([]),
    comments: Promise.resolve([]),
    friends: [],
    activity: [],
  });

  await userRepo.save(newAdmin);
  console.log('Seeded admin successfully!');
};

const seed = async () => {
  console.log('Seed started!');
  const connection = await createConnection();

  await seedRoles(connection);
  await seedBanStatus(connection);
  await seedAdmin(connection);

  await connection.close();
  console.log('Seed completed!');
};

seed().catch(console.error);
