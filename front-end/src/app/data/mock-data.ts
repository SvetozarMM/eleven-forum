import { Post } from '../models/post.model';
import { CommentDTO } from '../models/comment.dto';
import { UserDTO } from '../models/user.dto';

export const comments: CommentDTO[] = [
  {
    id: 'comment1',
    content: `Vitae et leo duis ut diam quam nulla porttitor.
    Netus et malesuada fames ac turpis egestas maecenas.`,
    createdOn: new Date(),
    owner: 'post1',
    userCreator: { username: 'Sam Johnson' },
    votes: ['user1', 'user2'],
  },
  {
    id: 'comment2',
    content: `Vitae et leo duis ut diam quam nulla porttitor.
    Netus et malesuada fames ac turpis egestas maecenas.`,
    createdOn: new Date(),
    owner: 'post1',
    userCreator: { username: 'Sam Johnson' },
    votes: ['user1', 'user2'],
  },
  {
    id: 'comment3',
    content: `Donec ac odio tempor orci dapibus. Fringilla urna porttitor rhoncus dolor.
    Eu tincidunt tortor aliquam nulla facilisi cras. Pellentesque dignissim enim sit amet.
    Metus dictum at tempor commodo ullamcorper. Augue neque gravida in fermentum et.`,
    createdOn: new Date(),
    owner: 'post2',
    userCreator: { username: 'John Doe' },
    votes: ['user1', 'user2'],
  },
  {
    id: 'comment4',
    content: `Donec ac odio tempor orci dapibus. Fringilla urna porttitor rhoncus dolor.
    Eu tincidunt tortor aliquam nulla facilisi cras. Pellentesque dignissim enim sit amet.
    Metus dictum at tempor commodo ullamcorper. Augue neque gravida in fermentum et.`,
    createdOn: new Date(),
    owner: 'post2',
    userCreator: { username: 'John Doe' },
    votes: ['user1', 'user2'],
  },
  {
    id: 'comment5',
    content: `Donec ac odio tempor orci dapibus. Fringilla urna porttitor rhoncus dolor.
    Eu tincidunt tortor aliquam nulla facilisi cras. Pellentesque dignissim enim sit amet.
    Metus dictum at tempor commodo ullamcorper. Augue neque gravida in fermentum et.`,
    createdOn: new Date(),
    owner: 'post2',
    userCreator: { username: 'Sara Smith' },
    votes: ['user1', 'user2'],
  },
];

export const users: UserDTO[] = [
  {
      id: 'user1',
      username: 'Sam Johnson',
      email: 'samjohanson@test.com',
      avatar: '../../../../assets/img/photoM.jpg',
      posts: ['post1', 'post2'],
      comments: ['comment1', 'comment2'],
      roles: ['Basic'],
      banstatus: false,
      likedPosts: ['post1'],
      likedComments: ['comment1', 'comment2', 'comment3'],
      friends: [],
  },
  {
      id: 'user2',
      username: 'John Doe',
      email: 'doe@test.com',
      avatar: '../../../../assets/img/photoM2.jpg',
      posts: ['post3'],
      comments: ['comment3'],
      roles: ['Admin'],
      banstatus: false,
      likedPosts: ['post1', 'post2', 'post3'],
      likedComments: ['comment1', 'comment2', 'comment3'],
      friends: [],
  }
];

export const posts: Post[] = [
  {
    id: 'post1',
    title: 'Beer 24/7',
    content: `Pretium lectus quam id leo. Leo urna molestie at elementum
    eu facilisis sed. Ullamcorper dignissim cras tincidunt lobortis feugiat. Est ante in nibh mauris cursus
    Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. In nulla posuere sollicitudin
    aliquam ultrices sagittis orci a. Facilisis magna etiam tempor orci.`,
    createdOn: new Date(),
    isLocked: false,
    comments: [comments[0], comments[1]],
    userCreator: users[0],
    votes: [users[1]],
  },
  {
    id: 'post2',
    title: 'Post, post',
    content: `Pretium lectus quam id leo. Leo urna molestie at elementum
    eu facilisis sed. Ullamcorper dignissim cras tincidunt lobortis feugiat. Est ante in nibh mauris cursus
    Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. In nulla posuere sollicitudin
    aliquam ultrices sagittis orci a. Facilisis magna etiam tempor orci.`,
    createdOn: new Date(),
    isLocked: false,
    comments: [comments[2]],
    userCreator: users[1],
    votes: [users[1]],
  },
  {
    id: 'post3',
    title: 'New post!',
    content: `Pretium lectus quam id leo. Leo urna molestie at elementum
    eu facilisis sed. Ullamcorper dignissim cras tincidunt lobortis feugiat. Est ante in nibh mauris cursus
    Maecenas volutpat blandit aliquam etiam erat velit scelerisque in dictum. In nulla posuere sollicitudin
    aliquam ultrices sagittis orci a. Facilisis magna etiam tempor orci.`,
    createdOn: new Date(),
    isLocked: false,
    comments: [comments[3], comments[4]],
    userCreator: users[1],
    votes: [users[0], users[1]],
  },
];


