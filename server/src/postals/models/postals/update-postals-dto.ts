import { IsString, Length } from 'class-validator';

export class UpdatePostDTO {
    @IsString()
    @Length(1, 100)
    title: string;

    @IsString()
    @Length(1)
    content: string;
}
