import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
import { AuthService } from '../../../core/services/auth.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.css']
})
export class AboutUsComponent implements OnInit, OnDestroy {

  private loggedInSubscription: Subscription;
  public loggedIn: boolean;

  constructor(
    private readonly authService: AuthService,
  ) {}

  public ngOnInit() {
    this.loggedInSubscription = this.authService.isLoggedIn$.subscribe(
      loggedIn => this.loggedIn = loggedIn,
    );
  }
  public ngOnDestroy() {
    this.loggedInSubscription.unsubscribe();
  }
}
