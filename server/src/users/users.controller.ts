// tslint:disable-next-line: max-line-length
import { Controller, Post, Body, Get, Query, UseGuards, HttpCode, HttpStatus, Put, Param, Delete, ParseIntPipe, UseInterceptors, UploadedFile } from '@nestjs/common';
import { ShowUserDTO } from './models/show-user.dto';
import { UserRole } from './enums/user-role.enum';
import { UsersService } from './users.service';
import { CreateUserDTO } from './models/create-user.dto';
import { FindUserDTO } from './models/find-user-dto';
import { AuthGuard } from '@nestjs/passport';
import { UpdateUserRolesDTO } from './models/update-user-roles.dto';
import { AdminGuard } from '../common/guards/admin.guard';
import { ResponseMessageDTO } from '../postals/models/response-message.dto';
import { UpdateUserBanStatusDTO } from './models/update-user-banstatus.dto';
import { BannedGuard } from '../common/guards/banned.guard';
import { User } from '../common/decorators/user.decorator';
import { UserEntity } from '../data/entities/user.entity';
import { PassportModule } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { UpdateAvatarDTO } from './models/update-user-avatar.dto';
import { ShowUserActivityDto } from './models/show-user-activity.dto';
import { UserActivityService } from './user-activity.service';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService, private readonly userActivityService: UserActivityService) {}

  @Get('all')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async returnUsers(
  ): Promise<ShowUserDTO[]> {
    return await this.usersService.allUsers();
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), BannedGuard)
  async returnUserByName(
    @Query() query: FindUserDTO,
  ): Promise<ShowUserDTO> {
    if (query.username) {
      return await this.usersService.findByName(query.username);
    }
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  async addNewUser(
    @Body() user: CreateUserDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.create(user, UserRole.Basic);
  }

  @Post(':id/avatar')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  @UseInterceptors(FileInterceptor('file'))
  public async uploadAvatar(
    @Param('id') id: string,
    @UploadedFile() file: any,
  ): Promise<ShowUserDTO> {
    const updateUserProps: UpdateAvatarDTO = {
      avatar: file.filename,
    };

    return await this.usersService.updateUser(id, updateUserProps);
  }

  @Put('/:userID/roles')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  public async updateUserRoles(
    @Param('userID') userID: string,
    @Body() updateUserRoles: UpdateUserRolesDTO,
  ): Promise<ShowUserDTO> {
    return await this.usersService.updateUserRoles(userID, updateUserRoles);
  }

  @Put('/:userID/banstatus')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async updateUserBanStatus(
      @Param('userID') userID: string,
      @Body() banstatus: UpdateUserBanStatusDTO,
    ): Promise<ShowUserDTO> {
    return await this.usersService.updateUserBanStatus(userID, banstatus);
  }

  @Delete()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuard('jwt'))
  async delete(
    @Query() query: FindUserDTO,
  ): Promise<ResponseMessageDTO> {
    await this.usersService.delete(query.username);

    return { message: `User deleted!` };
  }

  @Post(':userId/friends')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async addFriend(
    @Param('userId') friendId: string,
    @User() user: UserEntity,
  ): Promise<ShowUserDTO> {
    return await this.usersService.addFriend(friendId, user);
  }

  @Get('friends')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async getFriends(
  @User() user: UserEntity,
  ): Promise<ShowUserDTO[]> {
    return this.usersService.getFriends(user);
  }

  @Get(':userId/activity')
  @UseGuards(AuthGuard('jwt'))
  @HttpCode(HttpStatus.OK)
  public async getUserActivity(
    @User() user: UserEntity,
    @Param('userId') id: string,
    ): Promise<ShowUserActivityDto[]> {
     return await this.userActivityService.getUserActivity(id);
  }

}
