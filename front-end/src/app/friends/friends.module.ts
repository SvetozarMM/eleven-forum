import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FriendsComponent } from './components/friends/friends.component';
import { UsersModule } from '../users/users.module';
import { FormsModule } from '@angular/forms';
import { FriendBoxComponent } from './components/friend-box/friend-box.component';
import { AdminControlModule } from '../admin-control/admin-control.module';

@NgModule({
  declarations: [
    FriendsComponent,
    FriendBoxComponent,
  ],
  imports: [
    CommonModule,
    UsersModule,
    FormsModule,
    AdminControlModule,
  ]
})
export class FriendsModule { }
