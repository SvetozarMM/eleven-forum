import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('banstatus')
export class BanStatus {
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({type: 'nvarchar', default: ''})
    description: string;

    @Column({type: 'boolean', default: false})
    isBanned: boolean;
}
